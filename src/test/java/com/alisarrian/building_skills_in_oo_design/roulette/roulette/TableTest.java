package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class TableTest {
    private static final Outcome RED = new Outcome("Red", 1);
    private final Wheel wheel = new Wheel(new Random());
    private final Table table = new Table(wheel, 10, 300);

    @DisplayName("placeBet(bet) should place a valid bet on the table")
    @Test
    void should_place_valid_bet() throws InvalidBetException {
        Bet validBet = new Bet(30, RED);

        table.placeBet(validBet);

        assertNotNull(table.getBets());
        assertEquals(1, table.getBets().size());
    }

    @DisplayName("isValid() should check the correctness of placing bet with minimum amount")
    @Test
    void should_validate_minimum_bet() throws InvalidBetException {
        Bet minimumBet = new Bet(10, RED);

        table.placeBet(minimumBet);

        assertTrue(table.getBets().contains(minimumBet));
        assertEquals(1, table.getBets().size());
    }

    @DisplayName("isValid() should check the correctness of placing bet with minimum amount")
    @Test
    void should_validate_below_minimum_bet() {
        Bet belowMinimumBet = new Bet(5, RED);

        assertThrows(InvalidBetException.class,
                () -> table.placeBet(belowMinimumBet),
                "Do not add a bet below the table minimum.");
    }

    @DisplayName("isValid() should check the correctness of placing bet with limit amount")
    @Test
    void should_validate_limit_bet() throws InvalidBetException {
        Bet limitBet = new Bet(300, RED);

        table.placeBet(limitBet);

        assertTrue(table.getBets().contains(limitBet));
        assertEquals(1, table.getBets().size());
    }

    @DisplayName("isValid() should check the correctness of placing bet with above limit amount")
    @Test
    void should_validate_above_limit_bet() {
        Bet aboveLimitBet = new Bet(305, RED);

        assertThrows(InvalidBetException.class,
                () -> table.placeBet(aboveLimitBet),
                "Do not add a bet above the table limit.");
    }

    @DisplayName("clearBets() should clear all the bets from the Table")
    @Test
    void should_clear_the_table() throws InvalidBetException {
        Bet validBet = new Bet(30, RED);

        table.placeBet(validBet);
        table.clearBets();

        assertTrue(table.getBets().isEmpty());
    }

    @DisplayName("method_name(parameter) should...")
    @Test
    void should_return_empty_Table_details() {
        assertEquals("", table.toString());
    }

    @DisplayName("method_name(parameter) should...")
    @Test
    void should_return_Table_details_in_proper_format() throws InvalidBetException {
        Bet validBet = new Bet(30, RED);

        table.placeBet(validBet);

        assertEquals("30 on [Red (1:1)]", table.toString());
    }
}