package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Stream;

import static com.alisarrian.building_skills_in_oo_design.roulette.roulette.BetOdds.*;
import static org.junit.jupiter.api.Assertions.*;

class BinBuilderTest {

    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("bet_names");

    private Random rng;
    private Wheel wheel;

    @BeforeEach
    void setUp() {
        rng = new Random();
        wheel = new Wheel(rng);
    }

    /**
     * Random(seed: 1) -> This generates the following 'random' numbers: 25,18,17,21,0,24,10,14,28,18
     */
    @DisplayName("buildBins() - check if Bin no. 25 contains correct Outcomes")
    @Test
    void should_return_all_outcomes_for_given_bin() {
        rng = new Random(1);
        wheel = new Wheel(rng);

        Bin checkedBin = wheel.next();
        Set<Outcome> checkedBinOutcomes = checkedBin.getOutcomes();

        assertNotNull(checkedBin);
        assertEquals(OUTCOMES_IN_BIN.get(wheel.getBins().indexOf(checkedBin)).intValue(), checkedBinOutcomes.size(),
                "Check for bin index: " + wheel.getBins().indexOf(checkedBin));

        for (Outcome outcome : checkedBinOutcomes) {
            assertTrue(OUTCOMES_IN_BIN_25.contains(outcome.toString()));
        }
    }

    @DisplayName("buildBins() - check if Bin no. 0 contains correct Outcomes")
    @Test
    void should_return_all_bets_for_zero_bin() {
        final int binNumber = 0;
        Bin checkedBin = wheel.getBin(binNumber);
        Set<Outcome> checkedBinOutcomes = checkedBin.getOutcomes();

        assertNotNull(checkedBin);
        assertEquals(OUTCOMES_IN_BIN.get(wheel.getBins().indexOf(checkedBin)).intValue(), checkedBinOutcomes.size(),
                "Check for bin index: " + wheel.getBins().indexOf(checkedBin));

        for (Outcome outcome : checkedBinOutcomes) {
            assertTrue(OUTCOMES_IN_BIN_0.contains(outcome.toString()));
        }
    }

    @DisplayName("buildBins() - check if Bin no. 37 contains correct Outcomes")
    @Test
    void should_return_all_bets_for_double_zero_bin() {
        final int binNumber = 37;
        Bin checkedBin = wheel.getBin(binNumber);
        Set<Outcome> checkedBinOutcomes = checkedBin.getOutcomes();

        assertNotNull(checkedBin);
        assertEquals(OUTCOMES_IN_BIN.get(wheel.getBins().indexOf(checkedBin)).intValue(), checkedBinOutcomes.size(),
                "Check for bin index: " + wheel.getBins().indexOf(checkedBin));

        for (Outcome outcome : checkedBinOutcomes) {
            assertTrue(OUTCOMES_IN_BIN_37.contains(outcome.toString()));
        }
    }

    @DisplayName("buildBins() - check if each Bin contains a specific number of Outcomes")
    @Test
    void should_check_if_each_bin_has_expected_number_of_outcomes() {
        for (int i = 0; i < 38; i++) {
            Bin checkedBin = wheel.getBin(i);

            assertNotNull(checkedBin);
            assertTrue(checkedBin.getOutcomes().size() > 0);
            assertEquals(OUTCOMES_IN_BIN.get(i).intValue(), checkedBin.getOutcomes().size(),
                    "Check for bin index: " + i);
        }
    }

    @DisplayName("generateBets(wheel) should initiate each bin on the wheel with concrete Outcomes")
    @ParameterizedTest(name = "{index} => input={0}, binNumber={1}, expected={2}")
    @MethodSource("generatingBets_test_conditions")
    void should_generate_all_bets(Outcome input, int binNumber, boolean expected) {
        assertEquals(expected, wheel.getBin(binNumber).getOutcomes().contains(input));
    }

    /**
     * <number of bin, number of expected outcomes for the bin number>
     */
    private static final HashMap<Integer, Integer> OUTCOMES_IN_BIN;

    static {
        OUTCOMES_IN_BIN = new HashMap<>();
        OUTCOMES_IN_BIN.put(0, 2);
        OUTCOMES_IN_BIN.put(1, 12);
        OUTCOMES_IN_BIN.put(2, 14);
        OUTCOMES_IN_BIN.put(3, 12);
        OUTCOMES_IN_BIN.put(4, 14);
        OUTCOMES_IN_BIN.put(5, 17);
        OUTCOMES_IN_BIN.put(6, 14);
        OUTCOMES_IN_BIN.put(7, 14);
        OUTCOMES_IN_BIN.put(8, 17);
        OUTCOMES_IN_BIN.put(9, 14);
        OUTCOMES_IN_BIN.put(10, 14);
        OUTCOMES_IN_BIN.put(11, 17);
        OUTCOMES_IN_BIN.put(12, 14);
        OUTCOMES_IN_BIN.put(13, 14);
        OUTCOMES_IN_BIN.put(14, 17);
        OUTCOMES_IN_BIN.put(15, 14);
        OUTCOMES_IN_BIN.put(16, 14);
        OUTCOMES_IN_BIN.put(17, 17);
        OUTCOMES_IN_BIN.put(18, 14);
        OUTCOMES_IN_BIN.put(19, 14);
        OUTCOMES_IN_BIN.put(20, 17);
        OUTCOMES_IN_BIN.put(21, 14);
        OUTCOMES_IN_BIN.put(22, 14);
        OUTCOMES_IN_BIN.put(23, 17);
        OUTCOMES_IN_BIN.put(24, 14);
        OUTCOMES_IN_BIN.put(25, 14);
        OUTCOMES_IN_BIN.put(26, 17);
        OUTCOMES_IN_BIN.put(27, 14);
        OUTCOMES_IN_BIN.put(28, 14);
        OUTCOMES_IN_BIN.put(29, 17);
        OUTCOMES_IN_BIN.put(30, 14);
        OUTCOMES_IN_BIN.put(31, 14);
        OUTCOMES_IN_BIN.put(32, 17);
        OUTCOMES_IN_BIN.put(33, 14);
        OUTCOMES_IN_BIN.put(34, 11);
        OUTCOMES_IN_BIN.put(35, 13);
        OUTCOMES_IN_BIN.put(36, 11);
        OUTCOMES_IN_BIN.put(37, 2);
    }

    private static final List<String> OUTCOMES_IN_BIN_0 = List.of(
            "Straight 0 (35:1)",
            "Top Five 00-0-1-2-3 (6:1)"
    );

    private static final List<String> OUTCOMES_IN_BIN_25 = List.of(
            "High (1:1)",
            "Odd (1:1)",
            "Red (1:1)",
            "Column 1 (2:1)",
            "Dozen 3 (2:1)",
            "Line 22-23-24-25-26-27 (5:1)",
            "Line 25-26-27-28-29-30 (5:1)",
            "Corner 22-23-25-26 (8:1)",
            "Corner 25-26-28-29 (8:1)",
            "Street 25-26-27 (11:1)",
            "Split 22-25 (17:1)",
            "Split 25-26 (17:1)",
            "Split 25-28 (17:1)",
            "Straight 25 (35:1)"
    );

    private static final List<String> OUTCOMES_IN_BIN_37 = List.of(
            "Straight 00 (35:1)",
            "Top Five 00-0-1-2-3 (6:1)"
    );

    /**
     * The method provides test cases for each of the private generating methods. Created Outcome class objects
     * are placed in specified Bin class objects. One test set (for every generating method) consists of
     * first in range element, last in range+ 1-3 in the middle of the range.
     *
     * @return Stream<Arguments> of test cases
     */
    private static Stream<Arguments> generatingBets_test_conditions() {
        return Stream.of(
                /*Straight bets*/
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("straight_bet_name") + " 0", STRAIGHT.getOdds()), 0, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("straight_bet_name") + " 1", STRAIGHT.getOdds()), 1, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("straight_bet_name") + " 11", STRAIGHT.getOdds()), 11, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("straight_bet_name") + " 36", STRAIGHT.getOdds()), 36, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("straight_bet_name") + " 00", STRAIGHT.getOdds()), 37, true),

                /*Split bets*/
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 0-1", SPLIT.getOdds()), 0, false),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 1-2", SPLIT.getOdds()), 1, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 1-4", SPLIT.getOdds()), 1, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 1-5", SPLIT.getOdds()), 1, false),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 2-3", SPLIT.getOdds()), 3, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 3-6", SPLIT.getOdds()), 3, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 3-5", SPLIT.getOdds()), 3, false),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 31-34", SPLIT.getOdds()), 34, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 34-35", SPLIT.getOdds()), 34, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 32-34", SPLIT.getOdds()), 34, false),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 33-36", SPLIT.getOdds()), 36, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 35-36", SPLIT.getOdds()), 36, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 32-36", SPLIT.getOdds()), 36, false),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 1-5", SPLIT.getOdds()), 5, false),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 2-5", SPLIT.getOdds()), 5, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 3-5", SPLIT.getOdds()), 5, false),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 4-5", SPLIT.getOdds()), 5, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 5-5", SPLIT.getOdds()), 5, false),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 5-6", SPLIT.getOdds()), 5, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 5-7", SPLIT.getOdds()), 5, false),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 5-8", SPLIT.getOdds()), 5, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 5-9", SPLIT.getOdds()), 5, false),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " 00-3", SPLIT.getOdds()), 37, false),

                /*Street Bets*/
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("street_bet_name") + " 1-2-3", STREET.getOdds()), 1, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("street_bet_name") + " 1-2-3", STREET.getOdds()), 2, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("street_bet_name") + " 1-2-3", STREET.getOdds()), 3, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("street_bet_name") + " 1-2-3", STREET.getOdds()), 4, false),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("street_bet_name") + " 34-35-36", STREET.getOdds()), 33, false),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("street_bet_name") + " 34-35-36", STREET.getOdds()), 34, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("street_bet_name") + " 34-35-36", STREET.getOdds()), 35, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("street_bet_name") + " 34-35-36", STREET.getOdds()), 36, true),

                /*Corner bets*/
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 1-2-4-5", CORNER.getOdds()), 1, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 1-2-4-5", CORNER.getOdds()), 2, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 1-2-4-5", CORNER.getOdds()), 4, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 1-2-4-5", CORNER.getOdds()), 5, true),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 2-3-5-6", CORNER.getOdds()), 2, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 2-3-5-6", CORNER.getOdds()), 3, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 2-3-5-6", CORNER.getOdds()), 5, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 2-3-5-6", CORNER.getOdds()), 6, true),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 31-32-34-35", CORNER.getOdds()), 31, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 31-32-34-35", CORNER.getOdds()), 32, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 31-32-34-35", CORNER.getOdds()), 34, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 31-32-34-35", CORNER.getOdds()), 35, true),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 32-33-35-36", CORNER.getOdds()), 32, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 32-33-35-36", CORNER.getOdds()), 33, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 32-33-35-36", CORNER.getOdds()), 35, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " 32-33-35-36", CORNER.getOdds()), 36, true),

                /*Line Bets*/
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 1-2-3-4-5-6", LINE.getOdds()), 1, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 1-2-3-4-5-6", LINE.getOdds()), 2, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 1-2-3-4-5-6", LINE.getOdds()), 3, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 1-2-3-4-5-6", LINE.getOdds()), 4, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 1-2-3-4-5-6", LINE.getOdds()), 5, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 1-2-3-4-5-6", LINE.getOdds()), 6, true),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 4-5-6-7-8-9", LINE.getOdds()), 4, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 4-5-6-7-8-9", LINE.getOdds()), 5, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 4-5-6-7-8-9", LINE.getOdds()), 6, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 4-5-6-7-8-9", LINE.getOdds()), 7, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 4-5-6-7-8-9", LINE.getOdds()), 8, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 4-5-6-7-8-9", LINE.getOdds()), 9, true),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 28-29-30-31-32-33", LINE.getOdds()), 28, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 28-29-30-31-32-33", LINE.getOdds()), 29, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 28-29-30-31-32-33", LINE.getOdds()), 30, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 28-29-30-31-32-33", LINE.getOdds()), 31, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 28-29-30-31-32-33", LINE.getOdds()), 32, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 28-29-30-31-32-33", LINE.getOdds()), 33, true),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 31-32-33-34-35-36", LINE.getOdds()), 31, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 31-32-33-34-35-36", LINE.getOdds()), 32, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 31-32-33-34-35-36", LINE.getOdds()), 33, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 31-32-33-34-35-36", LINE.getOdds()), 34, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 31-32-33-34-35-36", LINE.getOdds()), 35, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " 31-32-33-34-35-36", LINE.getOdds()), 36, true),

                /*Dozen Bets*/
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " 1", DOZEN.getOdds()), 1, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " 1", DOZEN.getOdds()), 2, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " 1", DOZEN.getOdds()), 3, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " 1", DOZEN.getOdds()), 4, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " 1", DOZEN.getOdds()), 5, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " 1", DOZEN.getOdds()), 6, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " 1", DOZEN.getOdds()), 7, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " 1", DOZEN.getOdds()), 8, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " 1", DOZEN.getOdds()), 9, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " 1", DOZEN.getOdds()), 10, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " 1", DOZEN.getOdds()), 11, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " 1", DOZEN.getOdds()), 12, true),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " 1", DOZEN.getOdds()), 13, false),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " 2", DOZEN.getOdds()), 12, false),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " 2", DOZEN.getOdds()), 13, true),

                /*Column Bets*/
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " 1", COLUMN.getOdds()), 1, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " 1", COLUMN.getOdds()), 4, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " 1", COLUMN.getOdds()), 7, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " 1", COLUMN.getOdds()), 10, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " 1", COLUMN.getOdds()), 13, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " 1", COLUMN.getOdds()), 16, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " 1", COLUMN.getOdds()), 19, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " 1", COLUMN.getOdds()), 22, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " 1", COLUMN.getOdds()), 25, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " 1", COLUMN.getOdds()), 28, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " 1", COLUMN.getOdds()), 31, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " 1", COLUMN.getOdds()), 34, true),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " 1", COLUMN.getOdds()), 35, false),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " 2", COLUMN.getOdds()), 34, false),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " 2", COLUMN.getOdds()), 35, true),

                /*Even Money Bet: Low or High*/
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("low_bet_name"), LOW.getOdds()), 1, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("low_bet_name"), LOW.getOdds()), 16, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("low_bet_name"), LOW.getOdds()), 18, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("low_bet_name"), LOW.getOdds()), 19, false),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("high_bet_name"), HIGH.getOdds()), 18, false),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("high_bet_name"), HIGH.getOdds()), 19, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("high_bet_name"), HIGH.getOdds()), 20, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("high_bet_name"), HIGH.getOdds()), 21, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("high_bet_name"), HIGH.getOdds()), 36, true),

                /*Even Money Bet: Even or Odd*/
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("even_bet_name"), EVEN.getOdds()), 1, false),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("even_bet_name"), EVEN.getOdds()), 2, true),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("odd_bet_name"), ODD.getOdds()), 1, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("odd_bet_name"), ODD.getOdds()), 2, false),

                /*Even Money Bet: Red or Black*/
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("red_bet_name"), RED.getOdds()), 1, true),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("red_bet_name"), RED.getOdds()), 2, false),

                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("black_bet_name"), BLACK.getOdds()), 1, false),
                Arguments.of(new Outcome(RESOURCE_BUNDLE.getString("black_bet_name"), BLACK.getOdds()), 2, true)
        );
    }
}
































