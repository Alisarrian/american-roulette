package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static com.alisarrian.building_skills_in_oo_design.roulette.roulette.BetOdds.SPLIT;
import static org.junit.jupiter.api.Assertions.*;

class WheelTest {

    private static final int MAX_BINS = 38;
    private static final int BIN_NUMBER_1 = 1;
    private static final int BIN_NUMBER_40 = 40;

    private static final Outcome SPLIT_1_2  = new Outcome("Split 1-2", SPLIT.getOdds());

    private final Random randomGenerator = new Random(1);
    private Wheel wheel;

    @BeforeEach
    void setUp() {
        wheel = new Wheel(randomGenerator);
    }

    @DisplayName("initBins() should initiate proper number of bins ()")
    @Test
    void should_return_total_amount_of_bin() {
        assertEquals(MAX_BINS, wheel.getBins().size());
    }

    @DisplayName("addOutcome(outcome) should add new Outcome to the specific Bin")
    @Test
    void should_add_outcome_to_the_selected_bin() {
        wheel.addOutcome(BIN_NUMBER_1, SPLIT_1_2);

        assertTrue(wheel.getBin(BIN_NUMBER_1).getOutcomes().contains(SPLIT_1_2),
                "Successfully added Outcome should be present in Wheel.bins");
    }

    @DisplayName("addOutcome(outcome) should throw IllegalArgumentException when the binNumber is out of range")
    @Test
    void should_throw_IllegalArgumentException_when_binNumber_greater_than_MAX_BINS() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> wheel.addOutcome(BIN_NUMBER_40, SPLIT_1_2),
                "Bin number have to be in range of <0; 37>");
    }

    @DisplayName("addOutcome(outcome) should throw IllegalArgumentException when the Outcome is null")
    @Test
    void should_throw_IllegalArgumentException_when_given_outcome_is_null() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> wheel.addOutcome(BIN_NUMBER_1, null),
                "Outcome cannot be null.");
    }

    @DisplayName("get(binNumber) should return Bin object with given binNumber from Wheel.bins")
    @Test
    void should_return_bin_wih_given_number() {
        Bin bin = wheel.getBin(BIN_NUMBER_1);

        assertNotNull(bin);
    }

    @DisplayName("next() should return random Bin")
    @Test
    void should_return_a_random_Bin() {
        Bin bin = wheel.next();

        assertNotNull(bin);
    }

    @DisplayName("addOutcome(outcome) should add new Outcome to the map which contain all the Outcomes")
    @Test
    void should_add_outcome_to_the_allOutcomes() {
        wheel.addOutcome(BIN_NUMBER_1, SPLIT_1_2);

        assertTrue(wheel.getAllOutcomes().containsKey(SPLIT_1_2.getName()));
    }

    @DisplayName("getOutcomeByName(outcomeName) should should return specific Outcome object")
    @Test
    void should_return_outcome_by_name() {
        wheel.addOutcome(BIN_NUMBER_1, SPLIT_1_2);

        assertEquals(SPLIT_1_2, wheel.getOutcomeByName("Split 1-2"));
    }
}
