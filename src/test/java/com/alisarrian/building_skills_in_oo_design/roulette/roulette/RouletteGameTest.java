package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

import com.alisarrian.building_skills_in_oo_design.roulette.player.Martingale;
import com.alisarrian.building_skills_in_oo_design.roulette.player.Passenger57;
import com.alisarrian.building_skills_in_oo_design.roulette.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class RouletteGameTest {
    private final Random random = new Random(1);
    private final Wheel wheel = new Wheel(random);
    private final BinBuilder builder = new BinBuilder(wheel);
    private final Table table = new Table(wheel, 10, 300);
    private final RouletteGame game = new RouletteGame(wheel, table);

    private Player passenger57;

    @BeforeEach
    void setUp() {
        builder.buildBins();
        passenger57 = new Passenger57(500, table);
    }

    @DisplayName("spinFor(player) should complete single game cycle for given player" +
            "(Random used with seed, so the first random result is 25)")
    @Test
    void should_complete_single_game_cycle() {
        game.spinFor(passenger57);

        assertEquals(450, passenger57.getStake(),
                "Winning bin is Bin 25, so bet on black should lose.");
    }

    @DisplayName("spinFor(player) should complete single game cycle for Passenger57 player" +
            "(Random used with seed, so the first few random results are:" +
            "25 - lose, 18 - lose, 17 - win, 21 - lose, 0 - lose, 24 - win)")
    @Test
    void should_complete_six_game_cycles_for_passenger57() {
        for (int i = 0; i < 6; i++) {
            game.spinFor(passenger57);
        }

        assertEquals(400, passenger57.getStake(),
                "Player loses 4 times and wins 2 times," +
                        "so current balance of cash should be: 500 - 4*50 + 2*50 = 400.");
    }

    @DisplayName("spinFor(player) should complete single game cycle for Martingale player" +
            "(Random used with seed, so the first few random results are:" +
            "25 - lose, 18 - lose, 17 - win, 21 - lose, 0 - lose, 24 - win)")
    @Test
    void should_complete_six_game_cycles_for_Martingale() {
        Player martin = new Martingale(500, table);

        for (int i = 0; i < 6; i++) {
            game.spinFor(martin);
        }

        assertEquals(520, martin.getStake(),
                "Player loses 2 times and wins 1 time and then again - loses 2 times and wins 1 time," +
                        "so current balance of cash should be: 500 - 10 - 20 - 40 + 80 - 10 - 20 - 40 + 80 = 520.");
    }

    @DisplayName("spinFor(player) should return null when player is done playing")
    @Test
    void should_return_null_when_player_stop_playing() {
        Bin winningBin = new Bin();
        for (int i = 0; i < 12; i++) {
            winningBin = game.spinFor(passenger57);
            if (winningBin == null) break;
        }
        assertNull(winningBin);
    }
}