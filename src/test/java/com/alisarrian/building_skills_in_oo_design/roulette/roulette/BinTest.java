package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BinTest {
    private static final Outcome FIRST = new Outcome("first", 1);
    private static final Outcome SECOND = new Outcome("second", 2);

    private Bin testBin;

    @DisplayName("Creating object via default constructor and manually adding outcome")
    @Test
    void should_store_one_outcome() {
        testBin = new Bin();
        testBin.add(FIRST);

        assertEquals(1, testBin.getOutcomes().size(),
                "There should be one Outcome object in outcomes.");
        assertTrue(testBin.getOutcomes().contains(FIRST),
                "Bin should contains previously added object.");
    }

    @DisplayName("Creating object via overloaded constructor should initialize Bin.outcomes properly")
    @Test
    void should_store_two_outcomes_from_array_of_outcomes() {
        testBin = new Bin(new Outcome[]{FIRST, SECOND});

        assertEquals(2, testBin.getOutcomes().size(),
                "There should be one Outcome object in outcomes.");
        assertTrue(testBin.getOutcomes().contains(FIRST),
                "Bin should contains previously added object.");
        assertTrue(testBin.getOutcomes().contains(SECOND),
                "Bin should contains previously added object.");
    }

    @DisplayName("toString() should return Bin details in specific format with one Outcome in the Bin.outcomes")
    @Test
    void should_return_bin_details_in_proper_format_when_there_is_only_outcome_added() {
        testBin = new Bin(new Outcome[]{FIRST});

        assertEquals("[first (1:1)]", testBin.toString(),
                "Should outcome details in specific format (e.g. Split 1-2 (17:1)').");
    }
}
