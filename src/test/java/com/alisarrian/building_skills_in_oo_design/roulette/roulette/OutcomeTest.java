package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OutcomeTest {

    private Outcome out1;
    private Outcome out2;
    private Outcome out3;

    @BeforeEach
    void setUp() {
        out1 = new Outcome("1", 35);
        out2 = new Outcome("1", 35);
        out3 = new Outcome("Red", 17);
    }

    @DisplayName("equals(other) should return true if this name matches the other name")
    @Test
    void should_return_true_when_objects_have_same_name() {
        assertEquals(out1, out2);
    }

    @DisplayName("equals(other) should return true if objects have the same hashCode")
    @Test
    void should_return_true_when_objects_have_same_hashCode() {
        assertEquals(out1.hashCode(), out2.hashCode());
    }

    @DisplayName("equals(other) should return false if this name doesn't match the other name")
    @Test
    void should_return_false_when_objects_have_different_name() {
        assertNotEquals(out1, out3);
    }

    @DisplayName("equals(other) should return false if objects have different hashCodes")
    @Test
    void should_return_false_when_objects_have_different_hashCode() {
        assertNotEquals(out1.hashCode(), out3.hashCode());
    }

    @DisplayName("winAMount(amountBeingBet) should return amount won based on the outcome’s odds and the amount bet")
    @Test
    void should_calculate_won_amount() {
        assertEquals(350, out1.winAmount(10),
                "Amount won should by 350 (bet amount * odds -> 10*35).");
    }

    @DisplayName("winAmount(amountBeingBet) should throw IllegalArgumentException when amountBeingBet < 0")
    @Test
    void should_throw_IllegalArgumentException_when_amount_less_than_zero() {
        assertThrows(IllegalArgumentException.class,
                () -> out1.winAmount(-5),
                "Should have thrown an IllegalArgumentException, because amount to bet should not be less than 0."
        );
    }

    @DisplayName("winAmount(amountBeingBet) should throw IllegalArgumentException when amountBeingBet = 0")
    @Test
    void should_throw_IllegalArgumentException_when_amount_equals_zero() {
        assertThrows(IllegalArgumentException.class,
                () -> out1.winAmount(0),
                "Should have thrown an IllegalArgumentException, because amount to bet should not be less than 0."
        );
    }

    @DisplayName("toString() should return outcome details in specific format")
    @Test
    void should_return_outcome_details_in_proper_format() {
        assertEquals("1 (35:1)", out1.toString(),
                "Should outcome details in specific format (e.g. 1-2 Split (17:1)).");
    }
}