package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BetTest {
    private static final Outcome SPLIT_1_2 = new Outcome("Split 1-2", BetOdds.SPLIT.getOdds());
    private static final Outcome RED = new Outcome("Red", BetOdds.RED.getOdds());

    private static final Bet BET_ON_SPLIT = new Bet(100, SPLIT_1_2);
    private static final Bet BET_ON_RED = new Bet(57, RED);

    @DisplayName("winAmount() should calculate the correct value of the winning amount for bet set on Split")
    @Test
    void should_calculate_correct_value_of_winning_amount_when_bet_on_split() {
        assertEquals(1800, BET_ON_SPLIT.winAmount(),
                "The value of odds for bet \"Split 1-2\" is 17, the bet amount is 100," +
                        "so the winAmount should be 100*17+100 = 1800. ");
    }

    @DisplayName("winAmount() should calculate the correct value of the winning amount for bet set on Red")
    @Test
    void should_calculate_correct_value_of_winning_amount_when_bet_on_red() {
        assertEquals(114, BET_ON_RED.winAmount(),
                "The value of odds for bet \"Red\" is 1, the bet amount is 57," +
                        "so the winAmount should be 57*1+57 = 114. ");
    }

    @DisplayName("loseAmount() should calculate the correct value of the amount lost for bet set on Split")
    @Test
    void should_calculate_correct_value_of_lost_amount_when_bet_on_split() {
        assertEquals(100, BET_ON_SPLIT.loseAmount(),
                "The bet amount is 100, so the amount lost should be = 100. ");
    }

    @DisplayName("loseAmount() should calculate the correct value of the amount lost for bet set on Red")
    @Test
    void should_calculate_correct_value_of_lost_amount_when_bet_on_red() {
        assertEquals(57, BET_ON_RED.loseAmount(),
                "The bet amount is 57, so the amount lost should be = 57. ");
    }

    @DisplayName("toString() should return outcome details in specific format")
    @Test
    void should_return_bet_details_in_proper_format() {
        assertEquals("100 on [Split 1-2 (17:1)]", BET_ON_SPLIT.toString(),
                "Should outcome details in specific format (e.g. 1-2 Split (17:1)).");
    }
}