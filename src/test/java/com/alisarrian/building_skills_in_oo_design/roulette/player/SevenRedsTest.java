package com.alisarrian.building_skills_in_oo_design.roulette.player;

import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Bet;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Bin;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Table;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Wheel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ListIterator;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SevenRedsTest {
    private static final int RED_BIN_NUMBER = 25;
    private static final int BLACK_BIN_NUMBER = 26;
    private static final int SEVEN = 7;

    private final Random random = new Random(1);

    private Wheel wheel;
    private Table table;
    private Player sevenReds;

    private int timesToSpin;

    @BeforeEach
    void setUp() {
        wheel = new Wheel(random);
        table = new Table(wheel, 10, 200);
        sevenReds = new SevenReds(200, table);
    }

    @DisplayName("placeBets() should reject the first bet because this strategy involves 7 red bets " +
            "before placing the first bet on black")
    @Test
    void should_reject_the_first_bet() {
        timesToSpin = 1;
        spinWheelWithWinningBinNo(RED_BIN_NUMBER, timesToSpin);

        assertEquals(200, sevenReds.getStake(),
                "SevenReds should not placed any bet.");
    }

    @DisplayName("placeBets() should reject the sixth bet because this strategy involves 7 red bets " +
            "before placing the first bet on black")
    @Test
    void should_reject_the_sixth_bet() {
        timesToSpin = 6;
        spinWheelWithWinningBinNo(RED_BIN_NUMBER, timesToSpin);

        assertEquals(200, sevenReds.getStake(),
                "SevenReds should not placed any bet.");
    }

    @DisplayName("placeBets() should place a valid bet on Black, when 7 losers on Red. " +
            "But still Red is winning.")
    @Test
    void should_place_losing_black_bet() {
        timesToSpin = SEVEN + 1;
        spinWheelWithWinningBinNo(RED_BIN_NUMBER, timesToSpin);

        assertTrue(sevenReds.playing());
        assertEquals(190, sevenReds.getStake(),
                "SevenReds placed one valid bet but lost.");
    }

    @DisplayName("placeBets() should place a valid bet on Black, when 7 losers on Red. " +
            "But still Red is winning.")
    @Test
    void should_place_winning_black_bet() {
        timesToSpin = SEVEN;
        spinWheelWithWinningBinNo(RED_BIN_NUMBER, timesToSpin);
        spinWheelWithWinningBinNo(BLACK_BIN_NUMBER, 1);

        assertTrue(sevenReds.playing());
        assertEquals(210, sevenReds.getStake(),
                "SevenReds placed one winning bet.");
    }

    @DisplayName("placeBets() should place a two valid bets on Black (both lost)")
    @Test
    void should_place_two_black_bets() {
        timesToSpin = (SEVEN + 1) * 2;
        spinWheelWithWinningBinNo(RED_BIN_NUMBER, timesToSpin);

        assertEquals(170, sevenReds.getStake(),
                "Player loses 2 times, so current balance of cash should be: 200 - 10 - 20 = 170.");
    }

    @DisplayName("placeBets() should place a three valid bets on Black (first/second lost, third win)")
    @Test
    void should_place_three_black_bets() {
        timesToSpin = (SEVEN + 1) * 3;
        spinWheelWithWinningBinNo(RED_BIN_NUMBER, timesToSpin - 1);
        spinWheelWithWinningBinNo(BLACK_BIN_NUMBER, 1);

        assertEquals(210, sevenReds.getStake(),
                "Player loses 2 times and wins 1 times and then again - loses 2 times and wins 1 time," +
                        "so current balance of cash should be: 200 - 10 - 20 - 40 + 80 = 210.");
    }

    @DisplayName("placeBets() should place a six valid bets on Black (lose, lose, lose, win, lose, win)")
    @Test
    void should_place_six_black_bets() {
        timesToSpin = (SEVEN + 1) * 4;
        spinWheelWithWinningBinNo(RED_BIN_NUMBER, timesToSpin - 1);
        spinWheelWithWinningBinNo(BLACK_BIN_NUMBER, 1);

        spinWheelWithWinningBinNo(RED_BIN_NUMBER, SEVEN);
        spinWheelWithWinningBinNo(BLACK_BIN_NUMBER, 1);

        assertEquals(220, sevenReds.getStake(),
                "Player loses 2 times and wins 1 times and then again - loses 2 times and wins 1 time," +
                        "so current balance of cash should be: 200 - 10 - 20 - 40 - 80 + 160 - 10 - 20 + 40 = 210.");
    }

    private void spinWheelWithWinningBinNo(int binNumber, int times) {
        Bin currentBin = this.wheel.getBin(binNumber);
        for (int i = 0; i < times; i++) {
            sevenReds.placeBets();
            sevenReds.winners(currentBin.getOutcomes());
            checkAndResolvePlayerBets(sevenReds, currentBin);
            table.clearBets();
        }
    }

    private void checkAndResolvePlayerBets(Player player, Bin winningBin) {
        ListIterator<Bet> betListIterator = this.table.iterator();
        while (betListIterator.hasNext()) {
            Bet currentBet = betListIterator.next();

            if (winningBin.hasOutcome(currentBet.getOutcome())) {
                player.win(currentBet);
            } else {
                player.lose(currentBet);
            }
        }
    }
}