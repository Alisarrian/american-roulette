package com.alisarrian.building_skills_in_oo_design.roulette.player.player1326;

import com.alisarrian.building_skills_in_oo_design.roulette.player.Player;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Bet;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.BetOdds;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Bin;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Outcome;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Table;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Wheel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ListIterator;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class Player1326Test {
    private static final Bet BLACK_BET = new Bet(10, new Outcome("Black", BetOdds.BLACK.getOdds()));

    private Random rng = new Random();
    private Wheel wheel = new Wheel(rng);
    private Table table = new Table(wheel, 10, 300);

    private Player player;

    @BeforeEach
    void setUp() {
        player = new Player1326(200, table, 500);
    }

    @DisplayName("placeBets() should place valid Black Bet.")
    @Test
    void should_place_black_bet() {
        player.placeBets();

        assertEquals(190, player.getStake(),
                "Player1326 placed only one valid bet.");
    }

    @DisplayName("placeBets() should reject bet below minimum.")
    @Test
    void should_reject_bet_below_minimum() {
        player = new Player1326(5, table, 500);

        assertFalse(player.playing(),
                "Player1326 is not playing because there is not enough money even for one bet.");
    }

    @DisplayName("playing() should return false if Player has not enough money for at least minimal bet.")
    @Test
    void should_return_false_if_not_enough_money_for_minimal_bet() {
        player = new Player1326(10, table, 500);

        assertTrue(player.playing(), "Player1326 can play.");

        player.placeBets();

        assertEquals(0, player.getStake(),
                "Player1326 has not enough money for at least minimal bet, so he has to stop playing.");
        assertFalse(player.playing(), "Player1326 has not enough money to continue playing.");
    }

    @DisplayName("win(bet) should increase Player's stake with amount won.")
    @Test
    void should_increase_player_stake_when_bet_is_won() {
        player.placeBets();
        player.win(BLACK_BET);

        int expected = 200 - BLACK_BET.getAmountBet() + BLACK_BET.winAmount();
        assertEquals(expected, player.getStake());
    }

    @DisplayName("lose(bet) - If Bet was lost, the amount has already be taken while placing the bet.")
    @Test
    void should_decrease_player_stake_when_bet_is_lose() {
        player.placeBets();
        player.lose(BLACK_BET);

        int expected = 200 - BLACK_BET.loseAmount();
        assertEquals(expected, player.getStake());
    }

    @DisplayName("win(bet) should change state of the Player to Player1326OneWin when there was a first win.")
    @Test
    void should_change_state_to_Player1326OneWin_after_first_win() {
        int binNumber = 26;
        spinWheelWithWinningBinNo(binNumber, 1);

        assertEquals(Player1326OneWin.class, ((Player1326) player).getState().getClass());
    }


    @DisplayName("win(bet) should change Player status to Player1326NoWins when the result sequence was:" +
            "{win, lose}")
    @Test
    void should_change_state_to_Player1326NoWins_when_won_and_then_lost() {
        int binNumber = 26;
        spinWheelWithWinningBinNo(binNumber, 1);
        binNumber = 25;
        spinWheelWithWinningBinNo(binNumber, 1);

        assertEquals(Player1326NoWins.class, ((Player1326) player).getState().getClass());
    }

    @DisplayName("win(bet) should change Player status to Player1326TwoWins when the result sequence was:" +
            "{win, win}")
    @Test
    void should_change_state_to_Player1326TwoWins_after_second_win() {
        int binNumber = 26;
        spinWheelWithWinningBinNo(binNumber, 2);

        assertEquals(Player1326TwoWins.class, ((Player1326) player).getState().getClass());
    }

    @DisplayName("win(bet) should change Player status to Player1326ThreeWins when the result sequence was:" +
            "{win, win, win}")
    @Test
    void should_change_state_to_Player1326ThreeWins_after_third_win() {
        int binNumber = 26;
        spinWheelWithWinningBinNo(binNumber,3);

        assertEquals(Player1326ThreeWins.class, ((Player1326) player).getState().getClass());
    }

    @DisplayName("win(bet) should change Player status to Player1326NoWins when the result sequence was:" +
            "{win, win, win, win}")
    @Test
    void should_change_state_to_Player1326NoWins_after_the_forth_victory() {
        int binNumber = 26;
        spinWheelWithWinningBinNo(binNumber, 4);

        assertEquals(Player1326NoWins.class, ((Player1326) player).getState().getClass());
    }

    private void spinWheelWithWinningBinNo(int binNumber, int times) {
        Bin currentBin = this.table.getWheel().getBin(binNumber);
        for (int i = 0; i < times; i++) {
            this.player.placeBets();
            this.player.winners(currentBin.getOutcomes());
            checkAndResolvePlayerBets(currentBin);
            this.table.clearBets();
        }
    }

    private void checkAndResolvePlayerBets(Bin winningBin) {
        ListIterator<Bet> betListIterator = this.table.iterator();
        while (betListIterator.hasNext()) {
            Bet currentBet = betListIterator.next();

            if (winningBin.hasOutcome(currentBet.getOutcome())) {
                this.player.win(currentBet);
            } else {
                this.player.lose(currentBet);
            }
        }
    }
}