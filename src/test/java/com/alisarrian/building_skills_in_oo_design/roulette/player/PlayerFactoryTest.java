package com.alisarrian.building_skills_in_oo_design.roulette.player;

import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Table;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Wheel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PlayerFactoryTest {
    private final Random rng = new Random();
    private final Wheel wheel = new Wheel(rng);
    private final Table table = new Table(wheel, 10, 200);

    @DisplayName("createPlayer() should return a valid Passenger57 Player")
    @Test
    void should_return_valid_Passenger57() {
        Player passenger57 = PlayerFactory.createPlayer("Passenger57", 100, table, 100);

        assertNotNull(passenger57);
        assertEquals(100, passenger57.getStake());
    }

    @DisplayName("createPlayer() should return a valid Martingale Player")
    @Test
    void should_return_valid_Martingale() {
        Player martin = PlayerFactory.createPlayer("Martingale", 200, table, 100);

        assertNotNull(martin);
        assertEquals(200, martin.getStake());
    }

    @DisplayName("createPlayer() should return a valid SevenReds Player")
    @Test
    void should_return_valid_SevenReds() {
        Player sevenReds = PlayerFactory.createPlayer("SevenReds", 500, table, 100);

        assertNotNull(sevenReds);
        assertEquals(500, sevenReds.getStake());
    }

    @DisplayName("createPlayer() should return a valid PlayerRandom")
    @Test
    void should_return_valid_PlayerRandom() {
        Player randall = PlayerFactory.createPlayer("PlayerRandom", 125, table, 100);

        assertNotNull(randall);
        assertEquals(125, randall.getStake());
    }

    @DisplayName("createPlayer() should throw when unknown type of Player")
    @Test
    void should_throw_when_unknown_type() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> PlayerFactory.createPlayer("wrong_type", 100, null, 100));
    }

    @DisplayName("createPlayer() should throw when null as a type of Player")
    @Test
    void should_throw_when_null() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> PlayerFactory.createPlayer(null, 100, null, 100));
    }
}