package com.alisarrian.building_skills_in_oo_design.roulette.player;

import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Bet;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.BetOdds;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Outcome;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Table;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Wheel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class PlayerRandomTest {

    private static final Bet BLACK_BET = new Bet(10, new Outcome("Black", BetOdds.BLACK.getOdds()));

    private final Random wheelRng = new Random();
    private final Random playerRng = new Random();

    private Wheel wheel;
    private Table table;
    private Player randall;

    @BeforeEach
    void setUp() {
        wheel = new Wheel(wheelRng);
        table = new Table(wheel, 1, 200);
        randall = new PlayerRandom(100, table, playerRng);
    }

    @DisplayName("placeBets() should place valid random Bet")
    @Test
    void should_place_valid_random_bet() {
        randall.placeBets();

        assertEquals(90, randall.getStake(),
                "PlayerRandom placed one valid bet.");
    }

    @DisplayName("win(bet) should increase Player's stake with amount won")
    @Test
    void should_increase_player_stake_when_bet_is_won() {
        randall.placeBets();
        randall.win(BLACK_BET);

        int expected = 100 - BLACK_BET.getAmountBet() + BLACK_BET.winAmount();
        assertEquals(expected, randall.getStake());
    }

    @DisplayName("lose(bet) - If Bet was lost, the amount has already be taken while placing the bet.")
    @Test
    void should_decrease_player_stake_when_bet_is_lose() {
        randall.placeBets();
        randall.lose(BLACK_BET);

        int expected = 100 - BLACK_BET.loseAmount();
        assertEquals(expected, randall.getStake());
    }

    @DisplayName("playing() should return false if Player has not enough money for at least minimal bet")
    @Test
    void should_return_false_if_not_enough_money_for_minimal_bet() {
        table = new Table(wheel, 100, 200);
        randall = new PlayerRandom(50, table, playerRng);

        assertFalse(randall.playing(),
                "PlayerRandom is not playing because there is not enough money even for one bet.");
    }
}