package com.alisarrian.building_skills_in_oo_design.roulette.player;

import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Bet;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.BetOdds;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Outcome;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Table;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Wheel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class MartingaleTest {
    private static final Bet BLACK_BET = new Bet(1, new Outcome("Black", BetOdds.BLACK.getOdds()));

    private final Random random = new Random();

    private Wheel wheel;
    private Table table;
    private Player martin;

    @BeforeEach
    void setUp() {
        wheel = new Wheel(random);
        table = new Table(wheel, 1, 200);
        martin = new Martingale(200, table);
    }

    @DisplayName("placeBets() should place valid Black Bet")
    @Test
    void should_place_black_bet() {
        martin.placeBets();

        assertEquals(199, martin.getStake(),
                "Martingale placed only one valid bet.");
    }

    @DisplayName("placeBets() should place few valid Black Bets")
    @Test
    void should_place_few_black_bets() {
        martin.placeBets();
        assertEquals(199, martin.getStake(),
                "Martingale placed first valid bet.");

        martin.placeBets();
        assertEquals(198, martin.getStake(),
                "Martingale placed second valid bet.");
    }

    @DisplayName("placeBets() should reject bet below minimum")
    @Test
    void should_reject_bet_below_minimum() {
        table = new Table(wheel, 100, 200);
        martin = new Martingale(50, table);

        assertFalse(martin.playing(),
                "Martingale is not playing because there is not enough money even for one bet.");
    }

    @DisplayName("playing() should return false if Player has not enough money for at least minimal bet")
    @Test
    void should_return_false_if_not_enough_money_for_minimal_bet() {
        martin = new Martingale(1, table);

        assertTrue(martin.playing(), "Martingale can play.");

        martin.placeBets();

        assertEquals(0, martin.getStake(),
                "Martingale has not enough money for at least minimal bet, so he has to stop playing.");
        assertFalse(martin.playing(), "Martingale has not enough money to continue playing.");
    }

    @DisplayName("win(bet) should increase Player's stake with amount won")
    @Test
    void should_increase_player_stake_when_bet_is_won() {
        martin.placeBets();
        martin.win(BLACK_BET);

        int expected = 200 - BLACK_BET.getAmountBet() + BLACK_BET.winAmount();
        assertEquals(expected, martin.getStake());
    }

    @DisplayName("lose(bet) - If Bet was lost, the amount has already be taken while placing the bet.")
    @Test
    void should_decrease_player_stake_when_bet_is_lose() {
        martin.placeBets();
        martin.lose(BLACK_BET);

        int expected = 200 - BLACK_BET.loseAmount();
        assertEquals(expected, martin.getStake());
    }
}