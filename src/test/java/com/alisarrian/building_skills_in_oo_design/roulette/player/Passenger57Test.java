package com.alisarrian.building_skills_in_oo_design.roulette.player;

import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Bet;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.BetOdds;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.BinBuilder;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Outcome;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Table;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Wheel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class Passenger57Test {
    private static final Bet BLACK_BET = new Bet(50, new Outcome("Black", BetOdds.BLACK.getOdds()));

    private final Random random = new Random();
    private final Wheel wheel = new Wheel(random);
    private final BinBuilder builder = new BinBuilder(wheel);
    private Player passenger57;

    @BeforeEach
    void setUp() {
        builder.buildBins();
        Table table = new Table(wheel, 10, 200);
        passenger57 = new Passenger57(55, table);
    }

    @DisplayName("placeBets() should place valid Black Bet")
    @Test
    void should_place_black_bet() {
        passenger57.placeBets();

        assertEquals(5, passenger57.getStake(),
                "Passenger57 has enough money for just one valid bet.");
    }

    @DisplayName("placeBets() should reject bet below minimum")
    @Test
    void should_reject_bet_below_minimum() {
        Table table = new Table(wheel, 100, 200);
        passenger57 = new Passenger57(55, table);

        assertFalse(passenger57.playing(),
                "Passenger 57 is not playing because there is not enough money even for one bet.");

        passenger57.placeBets();
    }

    @DisplayName("playing() should return false if Player has not enough money for at least minimal bet")
    @Test
    void should_return_false_if_not_enough_money_for_minimal_bet() {
        assertTrue(passenger57.playing(), "Passenger57 can play.");

        passenger57.placeBets();

        assertEquals(5, passenger57.getStake(),
                "Passenger57 has not enough money for at least minimal bet, so he has to stop playing.");
        assertFalse(passenger57.playing(), "Passenger57 has not enough money to continue playing.");
    }

    @DisplayName("win(bet) should increase Player's stake with amount won")
    @Test
    void should_increase_player_stake_when_bet_is_won() {
        passenger57.win(BLACK_BET);

        assertEquals(55 + BLACK_BET.winAmount(), passenger57.getStake());
    }
}