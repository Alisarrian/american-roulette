package com.alisarrian.building_skills_in_oo_design.roulette.simulation.statistics;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IntegerStatisticsTest {
    private static final List<Integer> INTEGERS = List.of(9, 8, 5, 9, 9, 4, 5, 8, 10, 7, 8, 8);

    @DisplayName("mean(values) should calculate correct mean (average) from the elements of the given list")
    @Test
    void should_calculate_mean() {
        double expected = 7.5d;
        double result = IntegerStatistics.mean(INTEGERS);

        assertEquals(expected, result, .0001);
    }

    @DisplayName("stdev(values) should calculate correct standard deviation from the elements of the given list")
    @Test
    void should_calculate_st_dev() {
        double expected = 1.80277d;
        double result = IntegerStatistics.stdev(INTEGERS);

        assertEquals(expected, result, .0001);
    }

}