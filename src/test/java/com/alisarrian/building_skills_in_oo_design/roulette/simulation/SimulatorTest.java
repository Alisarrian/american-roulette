package com.alisarrian.building_skills_in_oo_design.roulette.simulation;

import com.alisarrian.building_skills_in_oo_design.roulette.roulette.RouletteGame;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Table;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Wheel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class SimulatorTest {
    private static final int DURATION = 20;
    private static final int SAMPLES = 25;

    private final Random rng = new Random();
    private final Wheel wheel = new Wheel(rng);
    private final Table table = new Table(wheel, 10, 200);
    private final RouletteGame theGame = new RouletteGame(wheel, table);

    @DisplayName("gather() should return proper data for Passenger57")
    @Test
    void should_gather_data_from_simulation_for_Passenger() {
        Map<String, List<Integer>> result = play(theGame, "Passenger57");

        checkResults(result);
    }

    @DisplayName("gather() should return proper data for Martingale")
    @Test
    void should_gather_data_from_simulation_for_Martingale() {
        Map<String, List<Integer>> result = play(theGame, "Martingale");

        checkResults(result);
    }

    @DisplayName("gather() should return proper data for SevenReds")
    @Test
    void should_gather_data_from_simulation_for_SevenReds() {
        Map<String, List<Integer>> result = play(theGame, "SevenReds");

        checkResults(result);
    }

    @DisplayName("gather() should return proper data for PlayerRandom")
    @Test
    void should_gather_data_from_simulation_for_PlayerRandom() {
        Map<String, List<Integer>> result = play(theGame, "PlayerRandom");

        checkResults(result);
    }

    private Map<String, List<Integer>> play(RouletteGame game, String playerType) {
        Simulator simulator = new Simulator(game, playerType, 100, DURATION, SAMPLES);
        return simulator.gather();
    }

    private void checkResults(Map<String, List<Integer>> result) {
        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.containsKey("durations"));
        assertTrue(result.containsKey("maxima"));
        result.values()
                .forEach((list) -> assertEquals(25, list.size()));
    }
}