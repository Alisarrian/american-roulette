package com.alisarrian.building_skills_in_oo_design.roulette.player;

import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Bet;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.InvalidBetException;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Outcome;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Table;

import java.util.Set;

/**
 * Stub class for tests.
 */
public class Passenger57 extends Player {

    /**
     * This is the outcome on which this player focuses their betting.
     */
    private final Outcome black;

    /**
     * Constructs the Player with a specific table for placing bets and with default (10) rounds to go.
     * This also creates the “black” Outcome (takes this Outcome from given Wheel instance).
     * This is saved in a variable named Passenger57.black for use in creating bets.
     *
     * @param stake the amount with which the player starts the game
     * @param table the Table instance on which bets are placed.
     */
    public Passenger57(int stake, Table table) {
        this(stake, table, 10);
    }

    /**
     * Constructs the Player with a specific table for placing bets.
     * This also creates the “black” Outcome (takes this Outcome from given Wheel instance).
     * This is saved in a variable named Passenger57.black for use in creating bets.
     *
     * @param stake      the amount with which the player starts the game
     * @param table      the Table instance on which bets are placed.
     * @param roundsToGo initial number of games
     */
    Passenger57(int stake, Table table, int roundsToGo) {
        super(stake, table, roundsToGo);
        black = table.getWheel().getOutcomeByName("Black");
    }

    /**
     * Updates the Table with the various bets. This version creates a Bet instance from the black
     * Outcome. It uses Table placeBet() to place that bet.
     */
    @Override
    public void placeBets() {
        Bet placedBet = new Bet(50, black);
        try {
            this.table.placeBet(placedBet);
        } catch (InvalidBetException e) {
            System.out.print("Passenger 57 failed to bet.");
            e.printStackTrace();
        }
        this.stake -= placedBet.getAmountBet();
    }

    /**
     * This method checks if the player has enough money for at least one bet, as well as the number of rounds
     * until the end of the simulation.
     *
     * @return Returns true while the player is still active.
     */
    @Override
    public boolean playing() {
        return (this.stake >= table.getMinimumBet() && this.roundsToGo > 0);
    }
}
