package com.alisarrian.building_skills_in_oo_design.roulette.player.player1326;

public class Player1326TwoWins extends Player1326State {
    /**
     * Player1326TwoWins defines the bet and state transition rules in the 1-3-2-6 betting system.
     * When there are two wins, the base bet value of 2 is used.
     *
     * @param player the player which will be used to provide the Outcome on which we will bet.
     */
    public Player1326TwoWins(Player1326 player) {
        super(player);
        this.setBetMultiplier(2);
    }

    /**
     * Constructs the new Player1326ThreeWins instance to be used when the bet was a winner.
     */
    @Override
    void nextWon() {
        this.player.setState(new Player1326ThreeWins(player));
    }
}
