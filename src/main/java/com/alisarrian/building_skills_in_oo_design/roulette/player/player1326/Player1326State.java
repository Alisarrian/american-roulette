package com.alisarrian.building_skills_in_oo_design.roulette.player.player1326;

import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Bet;

/**
 * Player1326State is the superclass for all of the states in the 1-3-2-6 betting system.
 */
public abstract class Player1326State {
    /**
     * Each subclass will set the appropriate value for this multiplier.
     * It is then used to create a Bet that is placed on the table.
     */
    private int betMultiplier;

    /**
     * The Player1326 player who is currently in this state. This player will be used to provide the Outcome
     * that will be used to create the Bet.
     */
    protected Player1326 player;

    /**
     * The constructor for this class saves the Player1326 which will be used to provide the Outcome
     * on which we will bet.
     *
     * @param player the player which will be used to provide the Outcome on which we will bet.
     */
    public Player1326State(Player1326 player) {
        this.player = player;
    }

    /**
     * Constructs a new Bet from the player’s preferred Outcome. Each subclass has a different multiplier
     * used when creating this Bet.
     *
     * @return new Bet with amount (from previous Bet) multiplied by the appropriate multiplier
     */
    public Bet currentBet() {
        return new Bet(player.getBetAmount() * betMultiplier, player.getOutcome());
    }

    /**
     * Constructs the new Player1326State instance to be used when the bet was a winner.
     */
    abstract void nextWon();

    /**
     * Constructs the new Player1326State instance to be used when the bet was a loser. This method
     * is the same for each subclass: it creates a new instance of Player1326NoWins.
     * This defined in the superclass to assure that it is available for each subclass.
     */
    void nextLost() {
        this.player.setState(new Player1326NoWins(player));
    }

    /**
     * This method is used by the subclasses to set appropriate value for the betMultiplier.
     *
     * @param betMultiplier value for the multiplier.
     */
    protected void setBetMultiplier(int betMultiplier) {
        this.betMultiplier = betMultiplier;
    }
}
