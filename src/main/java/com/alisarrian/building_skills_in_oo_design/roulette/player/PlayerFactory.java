package com.alisarrian.building_skills_in_oo_design.roulette.player;

import com.alisarrian.building_skills_in_oo_design.roulette.player.player1326.Player1326;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Table;

import java.util.Random;

public class PlayerFactory {

    /**
     * A factory that returns a specific Player class implementation based on the type provided.
     * @param type the type of Player (name of the class, Passenger57, Martingale, etc.)
     * @param initStake The stake value to use when initializing a Player
     * @param table The table on which the player will place bets
     * @param initDuration The duration value to use when initializing a Player
     * @return valid Player
     * @throws IllegalArgumentException when unknown type of Player or null
     */
    public static Player createPlayer(String type, int initStake, Table table, int initDuration) throws IllegalArgumentException {
        if (type == null) {
            throw new IllegalArgumentException("Wrong user type passed.");
        }

        switch (type) {
            case "Passenger57":
                return new Passenger57(initStake, table, initDuration);
            case "Martingale":
                return new Martingale(initStake, table, initDuration);
            case "SevenReds":
                return new SevenReds(initStake, table, initDuration);
            case "PlayerRandom":
                return new PlayerRandom(initStake, table, new Random(), initDuration);
            case "Player1326":
                return new Player1326(initStake, table, initDuration);
            default:
                throw new IllegalArgumentException("Wrong user type passed.");
        }
    }
}
