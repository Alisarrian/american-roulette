package com.alisarrian.building_skills_in_oo_design.roulette.player.player1326;

public class Player1326OneWin extends Player1326State {
    /**
     * Player1326OneWin defines the bet and state transition rules in the 1-3-2-6 betting system.
     * When there is one wins, the base bet value of 3 is used.
     *
     * @param player the player which will be used to provide the Outcome on which we will bet.
     */
    public Player1326OneWin(Player1326 player) {
        super(player);
        this.setBetMultiplier(3);
    }

    /**
     * Constructs the new Player1326TwoWins instance to be used when the bet was a winner.
     */
    @Override
    void nextWon() {
        this.player.setState(new Player1326TwoWins(player));
    }
}
