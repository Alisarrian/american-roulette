package com.alisarrian.building_skills_in_oo_design.roulette.player;

import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Bet;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.InvalidBetException;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Outcome;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * PlayerRandom is a Player who places bets in Roulette. This player makes random bets around the layout.
 */
public class PlayerRandom extends Player {
    /**
     * A Random Number Generator which will return the next random number.
     */
    private final Random rng;

    /**
     * A fixed amount of a single bet.
     */
    private final int betAmount = 10;

    /**
     * List of all possible Outcomes taken from the wheel.
     */
    private final List<Outcome> allOutcomes;

    /**
     * Constructs the Player with a specific Table for placing Bets.
     *
     * @param stake the cash to start with
     * @param table the table to use
     * @param rng   the random number generator
     */
    PlayerRandom(int stake, Table table, Random rng) {
        this(stake, table, rng, 10);
    }

    /**
     * Constructs the Player with initial stake with a specific Table to play
     * and with max rounds to play.
     *
     * @param stake      the cash to start with
     * @param table      the table to use
     * @param rng        the random number generator
     * @param roundsToGo initial number of games
     */
    PlayerRandom(int stake, Table table, Random rng, int roundsToGo) {
        super(stake, table, roundsToGo);
        this.rng = rng;
        this.allOutcomes = new ArrayList<>(this.table.getWheel().getAllOutcomes().values());
    }

    /**
     * @return Returns true while the player is still active.
     */
    @Override
    public boolean playing() {
        return (this.stake >= table.getMinimumBet() && this.roundsToGo > 0);
    }

    /**
     * Updates the Table with a randomly placed bet.
     */
    @Override
    public void placeBets() {
        int randomIndex = rng.nextInt(allOutcomes.size() - 1);
        Bet placedBet = new Bet(betAmount, allOutcomes.get(randomIndex));
        try {
            this.table.placeBet(placedBet);
        } catch (InvalidBetException e) {
            System.out.print("PlayerRandom failed to bet.");
            e.printStackTrace();
        }
        this.stake -= placedBet.getAmountBet();
    }
}
