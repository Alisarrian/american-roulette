package com.alisarrian.building_skills_in_oo_design.roulette.player;

import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Bet;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Outcome;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Table;

import java.util.Set;

public abstract class Player {

    /**
     * The player’s current stake. Initialized to the player’s starting budget.
     */
    protected int stake;

    /**
     * The number of rounds left to play
     */
    protected int roundsToGo;

    /**
     * The Table used to place individual Bets.
     */
    protected Table table;

    /**
     * Constructs the Player with a specific Table for placing Bets.
     *
     * @param stake the cash to start with
     * @param table the table to use
     */
    public Player(int stake, Table table) {
        this(stake, table, 10);
    }

    /**
     * Constructs the Player with initial stake with a specific Table to play
     * and with max rounds to play.
     *
     * @param stake      the cash to start with
     * @param table      the table to use
     * @param roundsToGo initial number of games
     */
    public Player(int stake, Table table, int roundsToGo) {
        this.stake = stake;
        this.table = table;
        this.roundsToGo = roundsToGo;
    }

    /**
     * @return Returns true while the player is still active.
     */
    public abstract boolean playing();

    /**
     * Updates the Table with the various bets.
     */
    public abstract void placeBets();

    /**
     * The game will notify a player of each spin using this method. This will be invoked even if the
     * player places no bets.
     *
     * It also decrement the number of rounds left to play.
     *
     * @param winners The set of Outcome instances that are part of the current win.
     */
    public void winners(Set<Outcome> winners) {
        this.roundsToGo--;
    }

    /**
     * Notification from the Game that the Bet was a winner. The amount of money won is available
     * via a the winAmount() method of theBet.
     *
     * @param bet The bet which won
     */
    public void win(Bet bet) {
        this.stake += bet.winAmount();
    }

    /**
     * Notification from the Game that the Bet was a loser.
     * Money has already been taken while placing the bet.
     *
     * @param bet The bet which lose.
     */
    public void lose(Bet bet) {
//        System.out.println("You lose.");
    }

    /**
     * Get info about Player's current balance of cash
     *
     * @return current balance of cash
     */
    public int getStake() {
        return this.stake;
    }
}
