package com.alisarrian.building_skills_in_oo_design.roulette.player.player1326;

import com.alisarrian.building_skills_in_oo_design.roulette.player.Player;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Bet;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.InvalidBetException;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Outcome;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Table;

public class Player1326 extends Player {

    /**
     * This is the player’s preferred Outcome. During construction, the Player must fetch this from the Wheel.
     */
    private final Outcome outcome;

    /**
     * This is the amount of the bet. At the beginning it is equal to the minimum table bet.
     */
    private final int betAmount;

    /**
     * This is the current state of the 1-3-2-6 betting system. It will be an instance of a subclass of
     * Player1326State. This will be one of the four states: No Wins, One Win, Two Wins or Three Wins.
     */
    private Player1326State state;

    /**
     * Constructs the Player with a specific table for placing bets. This also creates the “black” Outcome
     * (takes this Outcome from given Wheel instance). This is saved in a variable named Player1326.outcome
     * for use in creating bets.
     *
     * @param stake      the amount with which the player starts the game
     * @param table      the Table instance on which bets are placed.
     * @param roundsToGo initial number of games
     */
    public Player1326(int stake, Table table, int roundsToGo) {
        super(stake, table, roundsToGo);
        this.outcome = table.getWheel().getOutcomeByName("Black");
        this.betAmount = table.getMinimumBet();
        this.state = new Player1326NoWins(this);
    }

    /**
     * @return Returns true while the player is still active.
     */
    @Override
    public boolean playing() {
        return (this.stake >= table.getMinimumBet() && this.roundsToGo > 0);
    }

    /**
     * Updates the Table with a bet created by the current state.
     * This method delegates the bet creation to state object’s currentBet() method.
     */
    @Override
    public void placeBets() {
        Bet placedBet = this.state.currentBet();
        try {
            this.table.placeBet(placedBet);
        } catch (InvalidBetException e) {
            System.out.print("Player1326 failed to bet.");
        }
        this.stake -= placedBet.loseAmount();
    }

    /**
     * Notification from the Game that the Bet was a winner. The amount of money won is available
     * via a the winAmount() method of theBet.
     *
     * @param bet The bet which won
     */
    @Override
    public void win(Bet bet) {
        super.win(bet);
        this.state.nextWon();
    }

    /**
     * Notification from the Game that the Bet was a loser.
     * Money has already been taken while placing the bet.
     *
     * @param bet The bet which lose.
     */
    @Override
    public void lose(Bet bet) {
        super.lose(bet);
        this.state.nextLost();
    }

    /**
     * Setting the Player1326State property.The appropriate instance of the Player1326 subclass
     * will change this property depending on the conditions.
     *
     * @param state current state of the Player1326
     */
    protected void setState(Player1326State state) {
        this.state = state;
    }

    /**
     * Getter for this.state.
     *
     * @return the current state of the Player1326
     */
    public Player1326State getState() {
        return state;
    }

    /**
     * Getter for this.betAmount.
     *
     * @return the current bet amount
     */
    public int getBetAmount() {
        return betAmount;
    }

    /**
     * Getter for this.outcome
     *
     * @return the player’s preferred Outcome
     */
    public Outcome getOutcome() {
        return outcome;
    }
}
