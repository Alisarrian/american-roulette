package com.alisarrian.building_skills_in_oo_design.roulette.player;

import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Outcome;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Table;

import java.util.ResourceBundle;
import java.util.Set;

public class SevenReds extends Martingale {

    /**
     * Resource Bundle object which takes appropriate Outcome names from resource file.
     */
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("bet_names");

    /**
     * The number of reds yet to go. This starts at 7, is reset to 7 on each non-red outcome, and decrements
     * by 1 on each red outcome.
     */
    public int redCount = 7;

    /**
     * Constructs the Player with a specific table for placing bets and with default (10) rounds to go.
     * This also creates the “black” Outcome (takes this Outcome from given Wheel instance).
     * This is saved in a variable named Passenger57.black for use in creating bets.
     *
     * @param stake the amount with which the player starts the game
     * @param table the Table instance on which bets are placed.
     */
    public SevenReds(int stake, Table table) {
        super(stake, table);
    }

    /**
     * Constructs the Player with a specific table for placing bets.
     * This also creates the “black” Outcome (takes this Outcome from given Wheel instance).
     * This is saved in a variable named Passenger57.black for use in creating bets.
     *
     * @param stake      the amount with which the player starts the game
     * @param table      the Table instance on which bets are placed.
     * @param roundsToGo initial number of games
     */
    SevenReds(int stake, Table table, int roundsToGo) {
        super(stake, table, roundsToGo);
    }

    /**
     * Updates the Table with a bet on “black”. The amount bet is 2^lossCount, which is the value of
     * betMultiple.
     */
    @Override
    public void placeBets() {
        if (redCount == 0) {
            super.placeBets();
            this.redCount = 8;
        }
    }

    /**
     * The game will notify a player of each spin using this method. This will be invoked even if the
     * player places no bets.
     *
     * If this set includes red, redCount is decremented. Otherwise, redCount is reset to 7.
     *
     * @param winners The set of Outcome instances that are part of the current win.
     */
    @Override
    public void winners(Set<Outcome> winners) {
        Outcome red = this.table.getWheel().getOutcomeByName(RESOURCE_BUNDLE.getString("red_bet_name"));
        if (winners.contains(red)) {
            this.redCount--;
        } else {
            this.redCount = 7;
        }
        this.roundsToGo--;
    }
}
