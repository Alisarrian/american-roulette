package com.alisarrian.building_skills_in_oo_design.roulette.player;

import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Bet;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.InvalidBetException;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Outcome;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Table;

import java.util.Set;

/**
 * Martingale is a Player who places bets in Roulette. This player doubles their bet on every loss and resets
 * their bet to a base amount on each win.
 */
public class Martingale extends Player {

    /**
     * This is the outcome on which this player focuses their betting.
     */
    private final Outcome black;

    /**
     * This is the amount of the bet. At the beginning it is equal to the minimum table bet.
     */
    private final int betAmount;

    /**
     * The number of losses. This is the number of times to double the bet.
     */
    private int lossCount = 0;

    /**
     * The bet multiplier, based on the number of losses. This starts at 1, and is reset to 1 on each win.
     * It is doubled in each loss. This is always equal to 2^(lossCount) .
     */
    private int betMultiple = (int) Math.pow(2, lossCount);

    /**
     * Constructs the Player with a specific table for placing bets and with default (10) rounds to go.
     * This also creates the “black” Outcome (takes this Outcome from given Wheel instance).
     * This is saved in a variable named Passenger57.black for use in creating bets.
     *
     * @param stake the amount with which the player starts the game
     * @param table the Table instance on which bets are placed.
     */
    public Martingale(int stake, Table table) {
        this(stake, table, 10);
    }

    /**
     * Constructs the Player with a specific table for placing bets.
     * This also creates the “black” Outcome (takes this Outcome from given Wheel instance).
     * This is saved in a variable named Martingale.outcome for use in creating bets.
     *
     * @param stake      the amount with which the player starts the game
     * @param table      the Table instance on which bets are placed.
     * @param roundsToGo initial number of games
     */
    Martingale(int stake, Table table, int roundsToGo) {
        super(stake, table, roundsToGo);
        this.black = table.getWheel().getOutcomeByName("Black");
        this.betAmount = table.getMinimumBet();
    }

    /**
     * Uses the superclass win() method to update the stake with an amount won. This method then
     * resets lossCount to zero, and resets betMultiple to 1.
     *
     * @param bet The bet which won
     */
    @Override
    public void win(Bet bet) {
        super.win(bet);
        this.lossCount = 0;
        this.betMultiple = (int) Math.pow(2, lossCount);
    }

    /**
     * Notification from the Game that the Bet was a loser.
     * Money has already been taken while placing the bet.
     *
     * @param bet The bet which lose.
     */
    @Override
    public void lose(Bet bet) {
        super.lose(bet);
        this.lossCount++;
        this.betMultiple = (int) Math.pow(2, lossCount);
    }

    /**
     * Updates the Table with a bet on “black”. The amount bet is 2^lossCount, which is the value of
     * betMultiple.
     */
    @Override
    public void placeBets() {
        Bet placedBet = new Bet(betAmount * betMultiple, black);
        try {
            this.table.placeBet(placedBet);
        } catch (InvalidBetException e) {
            System.out.print("Martingale failed to bet.");
        }
        this.stake -= placedBet.loseAmount();
    }

    /**
     * This method checks if the player has enough money for at least one bet, as well as the number of rounds
     * until the end of the simulation, amount bet isn't greater than Table limit or Player available funds.
     *
     * @return Returns true while the player is still active.
     */
    @Override
    public boolean playing() {
        return (betAmount * betMultiple) <= stake
                && (betAmount * betMultiple) <= table.getTableLimit()
                && roundsToGo > 0
                && table.getMinimumBet() <= stake;
    }
}
