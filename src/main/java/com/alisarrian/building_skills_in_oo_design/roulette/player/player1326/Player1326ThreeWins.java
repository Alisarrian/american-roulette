package com.alisarrian.building_skills_in_oo_design.roulette.player.player1326;

public class Player1326ThreeWins extends Player1326State {
    /**
     * Player1326ThreeWins defines the bet and state transition rules in the 1-3-2-6 betting system.
     * When there are three wins, the base bet value of 6 is used.
     *
     * @param player the player which will be used to provide the Outcome on which we will bet.
     */
    public Player1326ThreeWins(Player1326 player) {
        super(player);
        this.setBetMultiplier(6);
    }

    /**
     * Constructs the new Player1326NoWins instance to be used when the bet was a winner.
     */
    @Override
    void nextWon() {
        this.player.setState(new Player1326NoWins(player));
    }
}
