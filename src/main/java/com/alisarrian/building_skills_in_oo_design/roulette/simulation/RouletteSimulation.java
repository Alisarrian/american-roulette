package com.alisarrian.building_skills_in_oo_design.roulette.simulation;

import com.alisarrian.building_skills_in_oo_design.roulette.roulette.RouletteGame;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Table;
import com.alisarrian.building_skills_in_oo_design.roulette.roulette.Wheel;
import com.alisarrian.building_skills_in_oo_design.roulette.simulation.statistics.StatisticPrinter;

import java.util.Random;

public class RouletteSimulation {

    public static void main(String[] args) {
        Random rng = new Random();
        Wheel wheel = new Wheel(rng);
        Table table = new Table(wheel, 10, 200);
        RouletteGame theGame = new RouletteGame(wheel, table);

        play(theGame, "Passenger57");

        play(theGame, "Martingale");

        play(theGame, "SevenReds");

        play(theGame, "PlayerRandom");

        play(theGame, "Player1326");
    }

    static void play(RouletteGame game, String playerType) {
        Simulator simulator = new Simulator(game, playerType, 1000, 200, 200);
        StatisticPrinter.printStatistics(playerType, simulator.gather());
        System.out.println();
    }
}
