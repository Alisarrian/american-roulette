package com.alisarrian.building_skills_in_oo_design.roulette.simulation.statistics;

import java.util.List;

/**
 * IntegerStatistics computes several simple descriptive statistics of Integer values in a List.
 */
public class IntegerStatistics {
    /**
     * Computes the mean of the List of Integer values.
     *
     * @param values The list of values we are summarizing.
     */
    public static double mean(List<Integer> values) {
        int sum = values.stream()
                .mapToInt(Integer::intValue)
                .sum();
        return values.isEmpty() ? 0.0d : (double) sum / values.size();
    }

    /**
     * Computes the standard deviation of the List of Integer values.
     *
     * @param values The list of values we are summarizing.
     */
    public static double stdev(List<Integer> values) {
        double mean = mean(values);
        double sqSum = values.stream()
                .mapToDouble(d -> Math.pow(d.doubleValue(), 2))
                .sum();
        double variance = sqSum / values.size() - Math.pow(mean, 2);
        return Math.sqrt(variance);
    }
}
