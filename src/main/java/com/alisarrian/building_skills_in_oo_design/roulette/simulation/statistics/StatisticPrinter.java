package com.alisarrian.building_skills_in_oo_design.roulette.simulation.statistics;

import java.util.List;
import java.util.Map;

public class StatisticPrinter {

    /**
     * Print simple statistics (mean/average and standard deviation
     *
     * @param statistics the map of maximum achieved stakes and maximum durations of the game
     */
    public static void printStatistics(String playerType, Map<String, List<Integer>> statistics) {
        System.out.println();
        System.out.println(">>----->STATISTICS<-----<<");
        System.out.print("Player: " + playerType);
        statistics.keySet()
                .forEach((key) -> System.out.printf("\n%s:\n\tMean (average): %f\n\tStandard deviation: %f",
                        key, IntegerStatistics.stdev(statistics.get(key)), IntegerStatistics.mean(statistics.get(key))
                ));
    }
}
