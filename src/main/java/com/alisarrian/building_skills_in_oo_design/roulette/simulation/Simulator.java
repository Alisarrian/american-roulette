package com.alisarrian.building_skills_in_oo_design.roulette.simulation;

import com.alisarrian.building_skills_in_oo_design.roulette.roulette.RouletteGame;
import com.alisarrian.building_skills_in_oo_design.roulette.player.Player;
import com.alisarrian.building_skills_in_oo_design.roulette.player.PlayerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Simulator {
    /**
     * The duration value to use when initializing a Player for a session
     */
    private final int initDuration;

    /**
     * the name of the Player class, to let the factory know which one to return
     */
    private final String playerType;

    /**
     * The stake value to use when initializing a Player for a session. This is a count of the number of bets
     * placed; i.e., 100 $10 bets is $1000 stake.
     */
    private final int initStake;

    /**
     * The number of game cycles to simulate.
     */
    private final int samples;

    /**
     * A List of lengths of time the Player remained in the game. Each session of play producrs a duration
     * metric, which are collected into this list.
     */
    private final List<Integer> durations = new LinkedList<>();

    /**
     * A List of maximum stakes for each Player. Each session of play producrs a maximum stake metric,
     * which are collected into this list.
     */
    private final List<Integer> maxima = new LinkedList<>();

    /**
     * The Player; essentially, the betting strategy we are simulating.
     */
    private Player player;

    /**
     * The casino game we are simulating. This is an instance of Game, which embodies the various rules, the
     * Table and the Wheel.
     */
    private final RouletteGame game;

    /**
     * Saves the Player and Game instances so we can gather statistics on the performance of the player’s
     * betting strategy. It also initiate Player's initial stake, duration of a game and the number of samples
     *
     * @param game         the Game we’re simulating. This includes the Table and Wheel.
     * @param playerType   the name of the Player class, to let the factory know which one to return
     * @param initStake    the amount with which the player starts the game.
     * @param initDuration the number of cycles to go in each session.
     * @param samples      the number of game cycles to simulate
     */
    public Simulator(RouletteGame game, String playerType, int initStake, int initDuration, int samples) {
        this.game = game;
        this.playerType = playerType;
        this.initStake = initStake;
        this.initDuration = initDuration;
        this.samples = samples;
    }

    /**
     * Executes the number of games sessions in samples. Each game session returns a List of stake
     * values. When the session is over (either the Player reached their time limit or their stake was
     * spent), then the length of the session List and the maximum value in the session List are the
     * resulting duration and maximum metrics. These two metrics are appended to the durations list
     * and the maxima list.
     * A client class will either display the durations and maxima raw metrics or produce statistical
     * summaries.
     */
    public Map<String, List<Integer>> gather() {
        List<Integer> stakes;
        for (int i = 0; i < samples; i++) {
            stakes = session();
            this.durations.add(stakes.size());
            this.maxima.add(getMaxStakeFrom(stakes));
        }
        return getRawData();
    }

    /**
     * Executes a single game session. The Player is initialized with their initial stake and initial cycles
     * to go.
     *
     * @return List of stake values.
     */
    private List<Integer> session() {
        try {
            this.player = PlayerFactory.createPlayer(playerType, initStake, game.getTable(), initDuration);
        } catch (Exception e) {
            System.out.println("Invalid player type.");
        }

        return getStakes();
    }

    /**
     * An empty List of stake values is created. The session loop executes until the Player
     * playing() returns false. This loop executes the Game cycle(); then it gets the stake from the
     * Player and appends this amount to the List of stake values. The List of individual stake values
     * is returned as the result of the session of play.
     *
     * @return List of stake values.
     */
    private List<Integer> getStakes() {
        List<Integer> stakes = new LinkedList<>();
        while (player.playing()) {
            game.spinFor(player);
            stakes.add(player.getStake());
        }

        return stakes;
    }

    /**
     * Find the maximum balance achieved by the player in each game session.
     *
     * @param list the list of Player's stakes from one session
     * @return the maximum balance achieved by the player
     */
    private Integer getMaxStakeFrom(List<Integer> list) {
        int max = Integer.MIN_VALUE;
        for (Integer i : list) {
            if (i > max) {
                max = i;
            }
        }
        return max;
    }

    /**
     * Collect raw simulation data.
     *
     * @return the map of maximum achieved stakes and maximum durations of the game
     */
    private Map<String, List<Integer>> getRawData() {
        return Map.of("maxima", this.maxima, "durations", this.durations);
    }
}
