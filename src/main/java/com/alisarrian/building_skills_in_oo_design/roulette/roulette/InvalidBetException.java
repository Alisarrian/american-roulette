package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

/**
 * InvalidBet is thrown when the Player attempts to place a bet which exceeds the table’s limit.
 */
public class InvalidBetException extends Exception {

    public InvalidBetException(String message) {
        super(message);
    }
}
