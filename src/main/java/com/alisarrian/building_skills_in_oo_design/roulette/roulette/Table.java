package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Table contains all the Bet s created by the Player. A table also has a betting limit, and the sum of all of
 * a player’s bets must be less than or equal to this limit. We assume a single Player in the simulation.
 */
public class Table {
    /**
     * the Wheel instance from which Outcomes are taken.
     */
    private final Wheel wheel;

    /**
     * Minimum bet.
     */
    private final int minimumBet;

    /**
     * This is the table limit. The sum of a Player ‘s bets must be less than or equal to this limit.
     */
    private final int tableLimit;

    /**
     * This is a List of the Bets currently active. These will result in either wins or losses to the Player.
     */
    private final List<Bet> bets;

    /**
     * Creates Table with an empty List of bets.
     *
     * @param wheel      the Wheel instance from which Outcomes are taken.
     * @param minimumBet minimum amount of bet.
     * @param tableLimit table limit.
     */
    public Table(Wheel wheel, int minimumBet, int tableLimit) {
        this.wheel = wheel;
        this.minimumBet = minimumBet;
        this.tableLimit = tableLimit;
        this.bets = new LinkedList<>();
    }

    /**
     * Adds this bet to the list of working bets. If the sum of all bets is greater than the table limit,
     * then an exception should be thrown.
     *
     * @param placedBet A Bet instance to be validated.
     */
    public void placeBet(Bet placedBet) throws InvalidBetException {
        if (!isValid(placedBet)) {
            throw new InvalidBetException(
                    String.format("Bet %1s with amount %2d, cannot be placed.", placedBet, placedBet.getAmountBet())
            );
        }
        this.bets.add(placedBet);
    }

    /**
     * Method that compares the total of a new prospective amount plus all existing Bet's to the table limit.
     * This can be used by the Player to evaluate each potential bet prior to creating it.
     *
     * @param currentBet bet to be validated
     * @return true if new bet can be placed
     */
    boolean isValid(Bet currentBet) {
        if (currentBet.getAmountBet() < minimumBet) {
            return false;
        }

        int sum = currentBet.getAmountBet() + getTotalOfPlacedBets();
        return sum <= tableLimit;
    }

    /**
     * Calculate the sum of all Bets currently placed on the Table.
     *
     * @return the sum of all Bets
     */
    private int getTotalOfPlacedBets() {
        return this.bets.stream()
                .mapToInt(Bet::getAmountBet)
                .sum();
    }

    /**
     * Returns a ListIterator over the list of bets. This gives us the freedom to change the representation
     * from LinkedList to any other Collection with no impact to other parts of the application.
     *
     * @return iterator over all bets
     */
    public ListIterator<Bet> iterator() {
        return this.bets.listIterator();
    }

    /**
     * Clears all the bets from the Table
     */
    public void clearBets() {
        this.bets.clear();
    }

    /**
     * Getter fot this.wheel
     *
     * @return the Wheel instance from which Outcomes are taken.
     */
    public Wheel getWheel() {
        return wheel;
    }

    /**
     * Getter fot this.minimumBet.
     *
     * @return the value of minimum bet on this Table
     */
    public int getMinimumBet() {
        return minimumBet;
    }

    /**
     * Getter fot this.tableLimit.
     *
     * @return the value of Table limit
     */
    public int getTableLimit() {
        return tableLimit;
    }

    /**
     * Getter for this.bets
     *
     * @return List of bets placed on the Table
     */
    public List<Bet> getBets() {
        return bets;
    }

    /**
     * @return String representation of all current bets
     */
    @Override
    public String toString() {
        final String delimiter = " | ";
        final StringBuilder sb = new StringBuilder();
        if (!bets.isEmpty()) {
            for (Bet bet : bets) {
                System.out.println(bet);
                sb.append(bet.toString());
                sb.append(delimiter);
            }
//            return sb.substring(0, sb.length() - delimiter.length());
        }
//        return sb.toString();
        return sb.length() > 0 ? sb.substring(0, sb.length() - delimiter.length()) : sb.toString();
    }
}
