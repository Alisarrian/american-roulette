package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

import com.alisarrian.building_skills_in_oo_design.roulette.player.Passenger57;
import com.alisarrian.building_skills_in_oo_design.roulette.player.Player;

import java.util.Random;

public class Main {


    public static void main(String[] args) throws InterruptedException {

        Wheel wheel = new Wheel(new Random());

        BinBuilder binBuilder = new BinBuilder(wheel);
        binBuilder.buildBins();

        Table table = new Table(wheel, 10, 500);

        RouletteGame theGame = new RouletteGame(wheel, table);

        Player passenger57 = new Passenger57(100, table);


        while (passenger57.getStake() > 0) {
            System.out.println("The wheel is spinning! No more bets!");
            Thread.sleep(500);
            Bin theWinningBin = theGame.spinFor(passenger57);

            System.out.println("The winning bets are: " + theWinningBin.toString());

            System.out.println("Player money: " + passenger57.getStake());
            System.out.println("Place your bets now!");
            Thread.sleep(500);
        }
        System.out.println("You are out of cash!");
    }
}
