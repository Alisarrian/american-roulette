package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Wheel contains the 38 individual bins on a Roulette wheel, plus a random number generator. It can select
 * a Bin at random, simulating a spin of the Roulette wheel.
 */
public class Wheel {
    /**
     * The game of Roulette centers around a wheel with thirty-eight numbered bins. The numbers include 0, 00
     * (double zero), 1 through 36. In total it is 38 Bin's for American version of Roulette.
     */
    private final int maxBins = 38;

    /**
     * Contains the individual Bin instances.
     */
    private List<Bin> bins;

    /**
     * The random number generator -> java.util.Random.
     */
    private final Random rng;

    /**
     * Map of the various Outcomes that are being assigned via the Wheel.addOutcome() method.
     */
    private final Map<String, Outcome> allOutcomes;

    /**
     * Creates a new wheel with 38 empty Bins. It will also create a new random number generator instance
     * and BinBuilder instance for initiating Bins.
     * For testing, this may be a non-random number generator.
     *
     * @param rng A “random” number generator.
     */
    public Wheel(Random rng) {
        this.rng = rng;
        initBins();
        this.allOutcomes = new HashMap<>();

        BinBuilder binBuilder = new BinBuilder(this);
        binBuilder.buildBins();
    }

    /**
     * This method creates the 'minBins' empty Bins.
     */
    private void initBins() {
        bins = new ArrayList<>();
        for (int i = 0; i < maxBins; i++) {
            bins.add(new Bin());
        }
    }

    /**
     * Generates a random number between 0 and this.maxBins, and returns the randomly selected Bin.
     *
     * @return A Bin selected at random from the wheel.
     */
    public Bin next() {
        return this.bins.get(rng.nextInt(maxBins));
    }

    /**
     * Method that returns an Outcome based on its name.
     *
     * @param outcomeName name of the Outcome to be returned
     * @return an Outcome based on its name
     */
    public Outcome getOutcomeByName(String outcomeName) {
        return this.allOutcomes.get(outcomeName);
    }

    /**
     * Adds the given Outcome to the Bin with the given number and to Map of all Outcomes
     *
     * @param binNumber bin number, in the range zero to (MAX_BINS-1) inclusive.
     * @param outcome   The Outcome to add to this Bin.
     */
    public void addOutcome(int binNumber, Outcome outcome) {
        addOutcomeToBin(binNumber, outcome);
        this.allOutcomes.put(outcome.getName(), outcome);
    }

    /**
     * Adds the given Outcome to the Bin with the given number.
     *
     * @param binNumber bin number, in the range zero to (MAX_BINS-1) inclusive.
     * @param outcome   The Outcome to add to this Bin.
     */
    private void addOutcomeToBin(int binNumber, Outcome outcome) {
        if (binNumber < 0 || binNumber > maxBins) {
            throw new IllegalArgumentException("Bin number should be in the range zero to " + (maxBins - 1) + " inclusive.");
        }
        if (outcome == null) {
            throw new IllegalArgumentException("The Outcome to add to this Bin should not be null.");
        }
        Bin bin = this.bins.get(binNumber);
        bin.add(outcome);
    }

    /**
     * Returns the given Bin from the internal collection.
     *
     * @param binNumber bin number, in the range zero to 37 inclusive.
     * @return The requested Bin or null if not found.
     */
    public Bin getBin(int binNumber) {
        return this.bins.get(binNumber);
    }

    /**
     * Returns list of bins.
     *
     * @return returns a list containing individual Bin instances
     */
    public List<Bin> getBins() {
        return this.bins;
    }

    /**
     * Returns map which contains all individual Outcomes
     *
     * @return map which contains all individual Outcomes
     */
    public Map<String, Outcome> getAllOutcomes() {
        return allOutcomes;
    }
}
