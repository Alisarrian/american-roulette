package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

import com.alisarrian.building_skills_in_oo_design.roulette.player.Player;

import java.util.ListIterator;

/**
 * Game manages the sequence of actions that defines the game of Roulette. This includes notifying the Player
 * to place bets, spinning the Wheel and resolving the Bets actually present on the Table.
 */
public class RouletteGame {

    /**
     * The Wheel that returns a randomly selected Bin of Outcomes.
     */
    private final Wheel wheel;

    /**
     * The Table which contains the Bets placed by the Player.
     */
    private final Table table;

    /**
     * Constructs a new Game, using a given Wheel and Table.
     *
     * @param wheel The wheel which produces random events.
     * @param table The table which holds bets to be resolved.
     */
    public RouletteGame(Wheel wheel, Table table) {
        this.wheel = wheel;
        this.table = table;
    }

    /**
     * This will execute a single cycle of play with a given Player.
     *
     * @param player the individual player that places bets, receives winnings and pays losses.
     * @return the winning Bin
     */
    public Bin spinFor(Player player) {
        if (!player.playing()) {
            return null;
        }
        player.placeBets();
        Bin winningBin = this.wheel.next();
        player.winners(winningBin.getOutcomes());
        checkAndResolvePlayerBets(player, winningBin);
        this.table.clearBets();

        return winningBin;
    }

    /**
     * This method iterates through all of Player's bets and resolve if they are winners or losers.
     *
     * @param player     The player whose bets are being resolved.
     * @param winningBin The winning Bin.
     */
    private void checkAndResolvePlayerBets(Player player, Bin winningBin) {
        ListIterator<Bet> betListIterator = this.table.iterator();
        while (betListIterator.hasNext()) {
            Bet currentBet = betListIterator.next();

            if (winningBin.hasOutcome(currentBet.getOutcome())) {
                player.win(currentBet);
            } else {
                player.lose(currentBet);
            }
        }
    }

    /**
     * Getter for this.table
     *
     * @return The table which holds bets to be resolved
     */
    public Table getTable() {
        return table;
    }
}
