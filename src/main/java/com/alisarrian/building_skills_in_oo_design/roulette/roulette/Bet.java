package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

/**
 * Bet associates an amount and an Outcome.
 */
public class Bet {

    /**
     * The amount of the bet.
     */
    private final int amountBet;

    /**
     * The Outcome on which the bet is placed.
     */
    private final Outcome outcome;

    /**
     * Create a new Bet of a specific amount on a specific outcome.
     *
     * @param amount  The amount of the bet.
     * @param outcome The Outcome we’re betting on.
     */
    public Bet(int amount, Outcome outcome) {
        this.outcome = outcome;
        this.amountBet = amount;
    }

    /**
     * Getter for the amount of the Bet.
     *
     * @return the amount of the Bet
     */
    public int getAmountBet() {
        return amountBet;
    }

    /**
     * Uses the Outcome‘s winAmount to compute the amount won, given the amount of this bet. Note
     * that the amount bet must also be added in. A 1:1 outcome (e.g. a bet on Red) pays the amount
     * bet plus the amount won.
     *
     * @return amount won
     */
    public int winAmount() {
        return this.outcome.winAmount(this.amountBet) + this.amountBet;
    }

    /**
     * Returns the amount bet as the amount lost. This is the cost of placing the bet.
     *
     * @return amount lost
     */
    public int loseAmount() {
        return this.amountBet;
    }

    /**
     * Getter for this.outcome
     * @return The Outcome on which the bet is placed
     */
    public Outcome getOutcome() {
        return outcome;
    }

    /**
     * Returns a string representation of this bet.
     *
     * @return string representation of this bet with the form ‘"amount on outcome"’
     */
    public String toString() {
        return this.amountBet + " on " + "[" + this.outcome.toString() + "]";
    }
}
