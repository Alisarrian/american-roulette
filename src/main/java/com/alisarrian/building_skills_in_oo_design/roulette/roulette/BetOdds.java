package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

/**
 * Numerator part for the odds
 *
 * <li>{@link #STRAIGHT}</li>
 * <li>{@link #SPLIT}</li>
 * <li>{@link #STREET}</li>
 * <li>{@link #CORNER}</li>
 * <li>{@link #LINE}</li>
 * <li>{@link #DOZEN}</li>
 * <li>{@link #COLUMN}</li>
 * <li>{@link #LOW}</li>
 * <li>{@link #HIGH}</li>
 * <li>{@link #RED}</li>
 * <li>{@link #BLACK}</li>
 * <li>{@link #EVEN}</li>
 * <li>{@link #ODD}</li>
 */
public enum BetOdds {
    STRAIGHT(35),
    SPLIT(17),
    STREET(11),
    CORNER(8),
    FIVE(6),
    LINE(5),
    DOZEN(2),
    COLUMN(2),
    LOW(1),
    HIGH(1),
    RED(1),
    BLACK(1),
    EVEN(1),
    ODD(1);

    /**
     * A value that reflects the value of the numerator part of odds. For given example: "35:1", value is "35"
     */
    private final int odds;

    /**
     * This method returns value for specific kind of Bet
     *
     * @return the numerator part of odds
     */
    public int getOdds() {
        return odds;
    }

    /**
     * Creates representation of the part of odd for Bets.
     *
     * @param odds the numerator part of odds
     */
    BetOdds(int odds) {
        this.odds = odds;
    }
}