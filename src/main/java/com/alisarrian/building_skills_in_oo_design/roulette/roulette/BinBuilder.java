package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

import java.util.Arrays;
import java.util.ResourceBundle;

import static com.alisarrian.building_skills_in_oo_design.roulette.roulette.BetOdds.*;

/**
 * BinBuilder creates the Outcomes for all of the 38 individual Bin on a Roulette wheel.
 */
public class BinBuilder {

    /**
     * Resource Bundle object which takes appropriate Outcome names from resource file.
     */
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("bet_names");

    /**
     * The numbers on the Roulette wheel which are red
     */
    private static final int[] RED_NUMBERS = {1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36};

    /**
     *  The Wheel with Bins that must be populated with Outcomes.
     */
    private final Wheel wheel;

    /**
     * Initializes the BinBuilder.
     *
     * @param wheel The Wheel with Bins that must be populated with Outcomes.
     */
    public BinBuilder(Wheel wheel) {
        this.wheel = wheel;
    }

    /**
     * Creates the Outcome instances and uses the addOutcome() method to place each Outcome in the
     * appropriate Bin of wheel.
     */
    public void buildBins() {
        generateStraightBets();
        generateSplitBets();
        generateStreetBets();
        generateCornerBets();
        generateLineBets();
        generateDozenBets();
        generateColumnBets();
        generateEvenMoneyBets();
    }

    /**
     * A “straight bet” is a bet on a single number. There are 38 possible bets, and they pay odds of 35 to 1.
     * Each bin on the wheel pays one of the straight bets.
     * <p>
     * Method generates "straight bets" for Bin from '0' to '36' inclusive. Last, '37' position is Bin '00' (double-zero).
     */
    private void generateStraightBets() {
        int lastBinIndex = wheel.getBins().size() - 1;
        for (int i = 0; i < lastBinIndex; i++) {
            wheel.addOutcome(i, new Outcome(RESOURCE_BUNDLE.getString("straight_bet_name") + " " + i, STRAIGHT.getOdds()));
        }
        wheel.addOutcome(lastBinIndex, new Outcome(RESOURCE_BUNDLE.getString("straight_bet_name") + " 00", STRAIGHT.getOdds()));
    }

    /**
     * A “split bet” is a bet on an adjacent pair of numbers. It pays 17:1. The table layout has the numbers
     * arranged sequentially in three columns and twelve rows. Adjacent numbers are in the same row or
     * column.
     * <p>
     * The four corners (1, 3, 34, and 36) only participate in two split bets. The center column of numbers
     * (5, 8, 11, ..., 32) each participate in four split bets. The remaining “edge” numbers participate in three
     * split bets. While this is moderately complex, the bulk of the layout (from 4 to 32) can be handled with
     * a simple rule to distinguish the center column from the edge columns.
     * <p>
     * The number 5 is adjacent to 4, 6, 2, 8; the number 1 is adjacent to 2 and 4.
     */
    private void generateSplitBets() {
        generateFiveBet();
        generateLeftRightSplitBets();
        generateUpDownSplitBets();
    }

    /**
     * Generate Five Bet Outcome (00-0-1-2-3, which pays 6:1).
     */
    private void generateFiveBet() {
        Outcome topFive = new Outcome(RESOURCE_BUNDLE.getString("top_five_bet_name") + " 00-0-1-2-3", FIVE.getOdds());
        wheel.addOutcome(0, topFive);
        wheel.addOutcome(1, topFive);
        wheel.addOutcome(2, topFive);
        wheel.addOutcome(3, topFive);
        wheel.addOutcome(37, topFive);
    }

    /**
     * We can generate the “left-right” split bets by iterating through the left two columns;
     * the numbers 1, 4, 7, ..., 34 and 2, 5, 8, ..., 35.
     */
    private void generateLeftRightSplitBets() {
        Outcome outcome;
        int binNumber;
        for (int rowNumber = 0; rowNumber < 12; rowNumber++) {
            binNumber = (3 * rowNumber) + 1;
            outcome = new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " " + binNumber + "-" + (binNumber + 1), SPLIT.getOdds());
            wheel.addOutcome(binNumber, outcome);
            wheel.addOutcome(binNumber + 1, outcome);

            binNumber++;
            outcome = new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " "  + binNumber + "-" + (binNumber + 1), SPLIT.getOdds());
            wheel.addOutcome(binNumber, outcome);
            wheel.addOutcome(binNumber + 1, outcome);
        }
    }

    /**
     * We can generate the “up-down” split bets by iterating through the numbers 1 through 33.
     * For each number, n, we generate a “n, n +3” split bet. This Outcome belongs to two Bins: n and n +3.
     */
    private void generateUpDownSplitBets() {
        Outcome outcome;
        for (int rowNumber = 1; rowNumber < 34; rowNumber++) {
            outcome = new Outcome(RESOURCE_BUNDLE.getString("split_bet_name") + " "  + rowNumber + "-" + (rowNumber + 3), SPLIT.getOdds());
            wheel.addOutcome(rowNumber, outcome);
            wheel.addOutcome(rowNumber + 3, outcome);
        }
    }

    /**
     * A “street bet” includes the three numbers in a single row, which pays 11:1. There are twelve of these
     * bets on the table. A single bin selects one street bet; any of three bins make a street bet a winner.
     */
    private void generateStreetBets() {
        Outcome outcome;
        int binNumber;
        for (int rowNumber = 0; rowNumber < 12; rowNumber++) {
            binNumber = (3 * rowNumber) + 1;
            outcome = new Outcome(RESOURCE_BUNDLE.getString("street_bet_name") + " "  + binNumber + "-" + (binNumber + 1) + "-" + (binNumber + 2),
                    STREET.getOdds());
            wheel.addOutcome(binNumber, outcome);
            wheel.addOutcome(binNumber + 1, outcome);
            wheel.addOutcome(binNumber + 2, outcome);
        }
    }

    /**
     * A square of four numbers is called a “corner bet” and pays 8:1. There are 72 of these bets available.
     * Each corner has four numbers, n , n +1, n +3, n +4. This is two numbers in the same row, and two numbers
     * in the next higher row.
     * Each number is a member of one, two or four corner bets. As with split bets, the bulk
     * of the layout can be handled with a simple rule to distinguish the column, and hence the “corners”. A
     * number in the center column (5, 8, 11, ..., 32) is a member of four corners. At the ends, 1, 3, 34, and
     * 36, are members of just one corner. All of the remaining numbers are along an edge and are members
     * of two corners.
     */
    private void generateCornerBets() {
        int binNUmber;
        for (int lineBetweenRow = 0; lineBetweenRow < 11; lineBetweenRow++) {
            binNUmber = (3 * lineBetweenRow) + 1;
            generateColumnCornerBetForBinNo(binNUmber);

            binNUmber++;
            generateColumnCornerBetForBinNo(binNUmber);
        }
    }

    /**
     * Each corner has four numbers, n , n +1, n +3, n +4. This is two numbers in the same row, and two numbers
     * in the next higher row.
     *
     * @param binNUmber Generate an Outcome object for Bin with this binNumber and its neighbors.
     */
    private void generateColumnCornerBetForBinNo(int binNUmber) {
        Outcome outcome =
                new Outcome(RESOURCE_BUNDLE.getString("corner_bet_name") + " "  + binNUmber + "-" + (binNUmber + 1) + "-" + (binNUmber + 3) + "-" + (binNUmber + 4), CORNER.getOdds());
        wheel.addOutcome(binNUmber, outcome);
        wheel.addOutcome(binNUmber + 1, outcome);
        wheel.addOutcome(binNUmber + 3, outcome);
        wheel.addOutcome(binNUmber + 4, outcome);
    }

    /**
     * A “line bet” is a six number block, which pays 5:1. It is essentially two adjacent street bets. There are
     * 11 such combinations.
     * For lines s numbered 0 to 10, the numbers on the line bet can be computed as follows: 3s+1, 3s+2, 3s+3,
     * 3s + 4, 3s + 5, 3s + 6. This Outcome object belongs to six individual Bins.
     */
    private void generateLineBets() {
        Outcome outcome;
        int binNumber;
        for (int lineBetweenRow = 0; lineBetweenRow < 11; lineBetweenRow++) {
            binNumber = (3 * lineBetweenRow) + 1;
            outcome = new Outcome(RESOURCE_BUNDLE.getString("line_bet_name") + " "  + binNumber
                            + "-" + (binNumber + 1)
                            + "-" + (binNumber + 2)
                            + "-" + (binNumber + 3)
                            + "-" + (binNumber + 4)
                            + "-" + (binNumber + 5),
                    LINE.getOdds());
            for (int i = 0; i < 6; i++) {
                wheel.addOutcome(binNumber + i, outcome);
            }
        }
    }

    /**
     * Any of the three 12-number ranges (1-12, 13-24, 25-36) pays 2:1. There are just three of these bets.
     */
    private void generateDozenBets() {
        for (int dozenNumber = 0; dozenNumber < 3; dozenNumber++) {
            generateBetsForDozenNo(dozenNumber);
        }
    }

    /**
     * Dozen bet Outcomes require enumerating all twelve numbers in each of three groups.
     *
     * @param dozen dozen number
     */
    private void generateBetsForDozenNo(int dozen) {
        Outcome outcome = new Outcome(RESOURCE_BUNDLE.getString("dozen_bet_name") + " "  + (dozen + 1), DOZEN.getOdds());
        for (int eachNumberInDozen = 0; eachNumberInDozen < 12; eachNumberInDozen++) {
            wheel.addOutcome(12 * dozen + eachNumberInDozen + 1, outcome);
        }
    }

    /**
     * The layout offers the three 12-number columns at 2:1 odds. All of the numbers in a given column
     * have the same remainder when divided by three. Column 1 contains 1, 4, 7, etc., all of which have a
     * remainder of 1 when divided by 3.
     * Column bet Outcomes require enumerating all twelve numbers in each of three groups. While the outline of
     * the algorithm is the same as the dozen bets, the enumeration of the individual numbers in the inner loop is
     * slightly different.
     */
    private void generateColumnBets() {
        Outcome outcome;
        for (int columnNumber = 0; columnNumber < 3; columnNumber++) {
            outcome = new Outcome(RESOURCE_BUNDLE.getString("column_bet_name") + " "  + (columnNumber + 1), COLUMN.getOdds());
            for (int rowNumber = 0; rowNumber < 12; rowNumber++) {
                wheel.addOutcome((3 * rowNumber) + columnNumber + 1, outcome);
            }
        }
    }

    /**
     * These include Red, Black, Even, Odd, High, Low. Each number has three of the six possible even money Outcome's.
     */
    private void generateEvenMoneyBets() {
        for (int binNumber = 1; binNumber < 37; binNumber++) {
            generateLowOrHighBet(binNumber);
            generateEvenOrOddBet(binNumber);
            generateRedOrBlackBet(binNumber);
        }
    }

    /**
     * There are two 18-number ranges: 1-18 is called low, 19-36 is called high. These are called even money
     * bets because they pay at 1:1 odds.
     *
     * @param binNumber check if Bin with this binNumber is Low Bet or High bet
     */
    private void generateLowOrHighBet(int binNumber) {
        wheel.addOutcome(binNumber,
                binNumber >= 1 && binNumber < 19
                        ? new Outcome(RESOURCE_BUNDLE.getString("low_bet_name"), LOW.getOdds())
                        : new Outcome(RESOURCE_BUNDLE.getString("high_bet_name"), HIGH.getOdds()));
    }

    /**
     * The individual numbers are colored red or black in an arbitrary pattern. Note that 0 and 00 are colored
     * green. The bets on red or black are even money bets, which pay at 1:1 odds.
     *
     * @param binNumber check if Bin with this binNumber is Red Bet or Black Bet
     */
    private void generateRedOrBlackBet(int binNumber) {
        wheel.addOutcome(binNumber,
                Arrays.binarySearch(RED_NUMBERS, binNumber) >= 0
                        ? new Outcome(RESOURCE_BUNDLE.getString("red_bet_name"), RED.getOdds())
                        : new Outcome(RESOURCE_BUNDLE.getString("black_bet_name"), BLACK.getOdds()));
    }

    /**
     * The numbers (other than 0 and 00) are also either even or odd. These bets are also even money bets.
     *
     * @param binNumber check if Bin with this binNumber is Even Bet or Odd Bet
     */
    private void generateEvenOrOddBet(int binNumber) {
        wheel.addOutcome(binNumber,
                binNumber % 2 == 0
                        ? new Outcome(RESOURCE_BUNDLE.getString("even_bet_name"), EVEN.getOdds())
                        : new Outcome(RESOURCE_BUNDLE.getString("odd_bet_name"), ODD.getOdds()));
    }
}
