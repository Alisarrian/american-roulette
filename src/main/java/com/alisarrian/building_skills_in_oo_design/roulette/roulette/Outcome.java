package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

import java.util.Objects;

/**
 * Outcome Model Object.
 *
 * @author Lukasz Bulczak
 */
public class Outcome implements Comparable<Outcome> {


    /**
     * Holds the name of the Outcome. Examples include "1", "Red".
     */
    private final String name;

    /**
     * Holds the payout odds for this Outcome.
     * Most odds are stated as 1:1 or 17:1, we only keep the numerator (17) and assume the denominator is 1.
     */
    private final int odds;

    /**
     * Constructor
     * Constructs a new Outcome with the specified name and payout odds for this outcome.
     *
     * @param name The name of this outcome.
     * @param odds The payout odds of this outcome.
     */
    public Outcome(String name, int odds) {
        this.name = name;
        this.odds = odds;
    }

    /**
     * Multiply this Outcome‘s odds by the given amount. The product is returned.
     *
     * @param amountBeingBet Amount being bet
     * @return amount won based on the outcome’s odds and the amount bet.
     * @throws IllegalArgumentException if amountBeingBet is less or equal zero.
     */
    public int winAmount(int amountBeingBet) {
        if (amountBeingBet <= 0) {
            throw new IllegalArgumentException();
        }
        return this.odds * amountBeingBet;
    }

    /**
     * Getter for Outcome.name
     *
     * @return this.name
     */
    public String getName() {
        return name;
    }

    /**
     * Compares two Outcome objects based on Outcome.name and Outcome.odds.
     *
     * @param o the Outcome to be compared.
     * @return the negative integer if this.name precedes the o.name
     * the positive integer if this.name follows the o.name
     * zero if their are equal.
     */
    @Override
    public int compareTo(Outcome o) {
        int result = Integer.compare(this.odds, o.odds);

        if (result == 0) {
            result = this.name.compareTo(o.name);
        }

        return result;
    }

    /**
     * Compare the name attributes of this and other.
     *
     * @param other Another Outcome to compare against.
     * @return true if this name matches the other name AND this odds os equal the other odds..
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (!(other instanceof Outcome)) return false;

        Outcome outcome = (Outcome) other;

        return odds == outcome.odds &&
                Objects.equals(name, outcome.name);
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, odds);
    }

    /**
     * Easy-to-read representation of this outcome.
     *
     * @return String of the form ‘name (odds:1)’.
     */
    @Override
    public String toString() {
        return name + " (" + odds + ":1)";
    }
}
