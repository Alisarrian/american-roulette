package com.alisarrian.building_skills_in_oo_design.roulette.roulette;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

/**
 * Bin Model Object.
 * The Roulette wheel has 38 bins, identified with a number and a color.
 * Each of these bins defines a number of closely related winning Outcome s.
 * <p>
 * Bin contains a collection of Outcomes which reflect the winning bets that are paid for a particular bin on a
 * Roulette wheel. In Roulette, each spin of the wheel has a number of Outcome s. Example: A spin of 1, selects
 * the “1” bin with the following winning Outcomes: “1” , “Red” , “Odd” , “Low” , “Column 1” , “Dozen 1-12”,
 * “Split 1-2” , “Split 1-4” , “Street 1-2-3” , “Corner 1-2-4-5” , “Five Bet” , “Line 1-2-3-4-5-6” , “00-0-1-2-3”,
 * “Dozen 1” , “Low” and “Column 1” . These are collected into a single Bin .
 */
public class Bin {

    /**
     * A Set that holds the connection of individual Outcomes.
     */
    private Set<Outcome> outcomes;

    /**
     * Default constructor
     * Creates an object and initialise.
     */
    public Bin() {
        this.outcomes = new TreeSet<>();
    }

    /**
     * Constructor
     * The array constructor will initialize the Bin.outcomes Set from an array of Outcomes.
     *
     * @param outcomes A collection of outcomes.
     */
    public Bin(Outcome[] outcomes) {
        this();
        this.outcomes = Set.of(outcomes);
    }

    /**
     * Adds an Outcome to this Bin.
     * This can be used by a builder to construct all of the bets in this Bin.
     *
     * @param outcome An outcome to add to this Bin
     */
    public void add(Outcome outcome) {
        this.outcomes.add(outcome);
    }

    /**
     * Check if this.outcomes contains specific Outcome
     * @param checkedOutcome Outcome to check
     * @return true if this.outcomes contains checkedOutcome
     */
    public boolean hasOutcome(Outcome checkedOutcome) {
        return this.outcomes.contains(checkedOutcome);
    }

    /**
     * Return unmodifiable set built from this.outcomes
     *
     * @return unmodifiableSet from outcomes
     */
    public Set<Outcome> getOutcomes() {
        return Collections.unmodifiableSet(this.outcomes);
    }

    /**
     * An easy-to-read representation of Bin.outcomes.
     *
     * @return String of the form ‘[outcome, outcome, ...]’.
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("[");
        this.outcomes.forEach(outcome -> sb.append(outcome).append(", "));
        return sb.toString().substring(0, sb.length() - 2).concat("]");
    }
}
